
$(document).ready(function(){

    if($(window).width()<=1024){
        $('#refine-filter').on('click',function(){
            $('.is-filters-opened .filter-toggle-text .filter-btn-open').css({
                'display': "none",
            }) 
            $(".filter-btn-open ").css({'display':''})
        })
        $('#filter-menubar').attr({'class':'col-lg-4 aside aside--left filter-col filter-mobile-col filter-col--sticky js-filter-col filter-col--init'})  
        $('body').attr({'class':'has-smround-btns has-loader-bg equal-height win documentLoad has-hdr_sticky no-loader is-fixed touch mac has-sticky'})  
        $('.is-filters-opened .filter-toggle-text .filter-btn-open').css({
            'display': "",
        }) 
        $(".filter-btn-open ").css({'display':'flex'})
    }

    var currentProducts=$('.listProduct').length;
    if(currentProducts<20){
        $('#loadMore').hide();
    }else{
        $('#loadMore').show();
    }
    // var url = "/get_product_count/"
    // fetch(url).then((response) => response.json()).then((data) => {
    //     console.log(data)
    // });

    $('#catColab').on('click', () => {
        $('#catshow').toggle(400)
    })
    $('#Brands').on('click', () => {
        $('#brandshow').toggle(400)
    })
    $('#VarientCat').on('click', () => {
        $('.VarientShow').toggle(400)
    })
    $('#priceColaps').on('click', () => {
        $('#priceChildColaps').toggle(300)
    })



    // filter Products
    $('.filter-checkbox').on('click',function(){
        var filterObj = {};
        $('.filter-checkbox').each(function(){
            var filterKey = $(this).data('filter');
            filterObj[filterKey] = Array.from(document.querySelectorAll('input[data-filter='+filterKey+']:checked')).map(function(e){
                 return e.value;
            });
        });
        filterObj['search_key'] = $(this).attr('data-search_key')
        filterObj['max_price'] = $("#max-slider").val()
        filterObj['min_price'] = $("#min-slider").val()
        console.log(filterObj);
        $.ajax({
            url:"/filterProductSearch/",
            data:filterObj,
            dataType:'json',
            beforeSend:()=>{
                console.log('Loading...')
            },
            success:(res)=>{
                console.log(res);
                //$('#ProdCantainer').text(res.data) ;
                $('#ProdCantainer').html(res.data) ;
                console.log(res.data)
                //loadbtn removed
                $('#loadMore').remove();
                $('.prd:not([class*="prd-w"])').css({'opacity':1})

            },
            error:(e)=>{
                alert(e);
            },
        });
    });


    //Product-Load-More
    $(document).on('click','#loadMore',function(){
        var currentProducts=$('.listProduct').length;
        var currentCategory = $(this).attr('data-cat');

        $.ajax({
            url : $(this).attr('data-url'),
            type:'post',
            data:{
                'offset':currentProducts,
                'cat':currentCategory,
                'csrfmiddlewaretoken':csrftoken
            },
            beforeSend:()=>{
                $(this).children('.span-text').text('Loading...')
                $(this).children('.spinner-grow-sm').addClass('spinner-grow')
                console.log('Loading...')
             
            },
            success:(res)=>{


                setTimeout(()=>{
                    var htmlData = `${res.data}`
                    $('.Product-container').append(htmlData);
                    $(this).children().removeClass('spinner-grow')
                    $(this).text('Load More')
                    
                    var afterappendCurrentProducts=$('.listProduct').length;
                    if(afterappendCurrentProducts==res.totalCount){
                        $(this).remove()
                    }
                },500);
            },
            error:(err)=>{
                console.log(err);
                alert(err);
            }
        });
    });

});


//varient price change
$(document).on('click','.getPrice',function () {
    var VarientId = $(this).data('id');
    var product = $(this).data('product');
    console.log('product id:',product)
    console.log('varient id:',VarientId)
    $.ajax({
        type: "GET",
        url: '/get_Prize/',
        data: {
            'subject_id': VarientId,
            // 'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },
        success: (data) => {
            var offerPrice = ((data.DisplayPrice - data.sellingPrice)*100)/data.DisplayPrice
            $('#prize'+product).html(`₹ ${data.sellingPrice} (${data.Value})`);
            $('#prizeD'+product).html(`₹ ${data.DisplayPrice}`);
            const offer = $(this).parent().parent().parent().parent().parent().children('.prd-img').children('.prd-big-squared-labels')
            if(offerPrice.toFixed(0) > 20){
                offer.html(`<div class="label-sale" style="background-color: #17c6aa;;">
                            <span>
                                ${offerPrice.toFixed(0)}%
                                <span class="sale-text">off</span>
                            </span>
                            <div class="countdown-circle">
                            </div>`)
            }else{
                offer.html('')
            }
            $('#CartBtn'+product).attr("data-id",data.Varientid);
            $('#CartBtn'+product).attr("data-item",data.Value);
            if(data.CartAddStatus==false || data.StockStatus==false){
                $('#CartBtn'+product).addClass('disabled')
            }else{
                $('#CartBtn'+product).removeClass('disabled')
            }
        }
    })
})

$(document).on('click','.getPrice1',function () {
    var VarientId = $(this).data('id');
    var product = $(this).data('product');
    console.log('product id:',product)
    console.log('varient id:',VarientId)
    $.ajax({
        type: "GET",
        url: '/get_Prize/',
        data: {
            'subject_id': VarientId,
        },
        success: (data) => {
            var offerPrice = ((data.DisplayPrice - data.sellingPrice)*100)/data.DisplayPrice
            const product_info = $(this).parent().parent().parent().parent().parent().siblings('.prd-info')
            const product_price = product_info.children('.prd-hovers').children('.prd-price')
            const offer = $(this).parent().parent().parent().parent().parent().children('.prd-img').children('.prd-big-squared-labels').children('.label-sale').children()
            if(offerPrice.toFixed(0) > 20){
                offer.html(`<div class="label-sale" style="background-color: #17c6aa;;">
                            <span>
                                ${offerPrice.toFixed(0)}%
                                <span class="sale-text">off</span>
                            </span>
                            <div class="countdown-circle">
                            </div>`)
            }else{
                offer.html('')
            }
            product_price.children('.price-old').children().html(`₹ ${data.DisplayPrice}`)
            product_price.children('.price-new').children().html(`₹ ${data.sellingPrice} (${data.Value})`)
            const CartEle = product_info.children('.prd-hovers').children('.prd-action').children().children()
            CartEle.attr({"data-id":data.Varientid,"data-item":data.Value})
            if(data.CartAddStatus==false || data.StockStatus==false){
                CartEle.addClass('disabled')
            }else{
                CartEle.removeClass('disabled')
            }
        }
    })
})

//wishlist
$(document).on('click','.Wishlist_add',function (e) {
    e.preventDefault()
    var DataId = $(this).attr('data-id')
    $.ajax({
        type: "GET",
        url: '/wishList/',
        data: {
            'subject_id': DataId,
            'csrfmiddlewaretoken': $('input[name=csrfmiddlewaretoken]').val(),
        },
        success: (res) => {
            if (res.succes == true) {
                $(this).html('<i class="icon-heart-hover">')
                $(this).attr({'title':'Remove From Cart'})
                $("#W_Count").html(res.count)
                $("#W_Count1").html(res.count)
            } else {
                $(this).html('<i class="icon-heart-stroke"></i>')
                $(this).attr({'title':'Add To Cart'})
                $("#W_Count").html(res.count)
                $("#W_Count1").html(res.count)
                if(parseInt(res.count)>=5){
                    alert('Maximum Limit exceed')
                }
            }
        },
        error: (error) => {
            document.location.href = "/loginpage/"
        }
    })

})
//cart
$(document).on('click','.AddToCart',function(e){
    e.preventDefault()
    var ProductVarientId=$(this).attr('data-id');
    $.ajax({
        type: "GET",
        url:"/Add_To_Cart/",
        data:{
            'VarientId':ProductVarientId,
        },
        success:(res)=>{
            if(res.status==true){
                $(this).addClass('disabled')
                $('#CartCount').html(res.cartCount)
                $('#finalPrize').html(`₹${res.final}`)
                $('#CartCount1').html(res.cartCount)
                $('#finalPrize1').html(`₹${res.final}`)
            }else{
                alert('Exist')
            }    
        },
        error: (error) => {
            document.location.href = "/loginpage/"
        }
    })
})


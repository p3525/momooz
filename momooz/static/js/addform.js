$(document).on('click', '.plus-varient-set-type-btn', function () {
    var htm = '     <div class="row" id="varient1">\
                        <div class="col-sm-3">\
                            <div class="form-group">\
                                <label for="exampleInputUsername1">{{form_1.Varient_name.label}}</label>\
                                {{form_1.Varient_name}}\
                            </div>\
                        </div>\
                        <div class="col-sm-3">\
                            <div class="form-group">\
                                <label for="exampleInputUsername1">Varient Values</label>\
                                <select name="Varient_Values" class="form-control form-control-sm" required=""\
                                    id="id_Varient_Values">\
                                    <option value="" selected=""></option>\
                                    <option value="1">1 kg</option>\
                                </select>\
                            </div>\
                        </div>\
                        <div class="col-sm-2">\
                            <div class="form-group">\
                                <label for="exampleInputUsername1">{{form_1.Selling_Prize.label}}</label>\
                                {{form_1.Selling_Prize}}\
                            </div>\
                        </div>\
                        <div class="col-sm-2">\
                            <div class="form-group">\
                                <label for="exampleInputUsername1">{{form_1.Display_Prize.label}}</label>\
                                {{form_1.Display_Prize}}\
                            </div>\
                        </div>\
                        <div class="col-sm-2">\
                            <div class="form-group" style="text-align: center;padding-top: 1.3em;">\
                                <button class="btn btn-link minus-varient-btn" type="button"\
                        style="margin: 0 auto;padding-top: 8%;"><i class="fa fa-minus"></i></button>\
                            </div>\
                        </div>\
                    </div> '

    var btn = '<button class="btn btn-link plus-varient-set-type-btn" type="button"\
                                    style="margin: 0 auto;padding-top: 8%;"><i class="fas fa-plus-square"\
                                        style="font-size: large;"></i></button>'
    $(this).after(btn);
    $(this).remove();
    $('.varient:first').after(htm);
})

$(document).on('click', '.minus-varient-btn', function () {
    $(this).parent().parent().parent().remove();
})
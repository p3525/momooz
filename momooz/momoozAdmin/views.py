import collections
import socket  
from django.contrib import messages
from django.contrib.admin.options import ModelAdmin
from django.db.models import Q
from django.http.response import Http404, HttpResponse, HttpResponseRedirect, JsonResponse,HttpResponseNotFound
from django.http import response
from django.template.loader import get_template
from django.shortcuts import redirect, render
from django.core.paginator import Paginator

# from xhtml2pdf import pisa
from momoozUser.models import UserLog,Contact_us
from .models import *
from .forms import *
from datetime import date
from django.core import serializers
from django.core.mail import send_mail
from datetime import datetime
import csv
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
from django.core.files.base import ContentFile
from datetime import date
import datetime as dt
import reportlab
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from io import BytesIO
from django.views import View
from .send_sms import send_SMS
fs=FileSystemStorage(location='temp/')

page_count=10


PROCESSED_CONTENT = "Your Order has been processed"
SHIPPED_CONTENT = "Your Order has ready for shipping"
ORDER_OUT_FOR_DELIVERY = "your order is out for delivery"
ORDER_DEIVERD = "your order has deliverd"

# def create_superuser( username, email=None, password=None, **extra_fields):
#         extra_fields.setdefault('is_staff', True)
#         extra_fields.setdefault('is_superuser', True)

#         if extra_fields.get('is_staff') is not True:
#             raise ValueError('Superuser must have is_staff=True.')
#         if extra_fields.get('is_superuser') is not True:
#             raise ValueError('Superuser must have is_superuser=True.')

#         return create_superuser(username, email, password, **extra_fields)



def loginPerson(request):
    if Admin.objects.get(id=request.session['loginId']).is_superuser:
        is_superuser = True
    else:
        is_superuser = False
    context={
        'loginPesron':Admin.objects.get(id=request.session['loginId']),
        'adminLogin':True,
        'is_superuser' : is_superuser,
        'contact_obj_render' : Contact_us.objects.all().filter(status=False).order_by('-date'),
        'message_count' : Contact_us.objects.all().filter(status=False).count()
    }
    return context

def has_permission(request):
    login_person = Admin.objects.get(id=request.session['loginId']).role.id
    permission = [ i.sub_path.path_name for i in set_permissions.objects.all().filter(role=login_person)]
    
    return permission

def Log_register(request,activity):
    hostname = socket.gethostname()    
    IPAddr = socket.gethostbyname(hostname) 
    current_user = Admin.objects.get(id=request.session['loginId']).username
    now = datetime.now()
    # current_time = now.strftime("%H:%M:%S")
    log_form = Log(user=current_user,activity=activity,date= now,ip_address=IPAddr)
    log_form.save()
    print("success")

def login(request):
    if 'loginId' in request.session:
        #return redirect('index')
        hostname = socket.gethostname()    
        IPAddr = socket.gethostbyname(hostname)    
        if request.method=="POST":
            username = request.POST['username']
            password = request.POST['password']
            if username and password:
                container = [i.username for i in Admin.objects.all()]
                if username in container:
                    
                    if password == Admin.objects.get(username=username).password:
                        request.session['loginId'] = Admin.objects.get(username=username).id
                        activity = "admin {} Logged in at {}".format(Admin.objects.get(id=request.session['loginId']).username,datetime.now())
                        Log_register(request,activity)
                        content = "You Are Logged in"
                        phone = "+919539288508"
                        send_SMS(content,phone)

                        return redirect('index')
                    else:
                        messages.error(request,"Wrong Password")
                        return render(request,'login.html')
                else:
                    messages.error(request,"Username doestn't exist")
                    return render(request,'login.html')
            else:
                messages.error(request,'empty field')
        return render(request,'login.html')
    else:
        hostname = socket.gethostname()    
        IPAddr = socket.gethostbyname(hostname)    
        if request.method=="POST":
            username = request.POST['username']
            password = request.POST['password']
            if username and password:
                container = [i.username for i in Admin.objects.all()]
                if username in container:
                    
                    if password == Admin.objects.get(username=username).password:
                        request.session['loginId'] = Admin.objects.get(username=username).id
                        activity = "admin {} Logged in at {}".format(Admin.objects.get(id=request.session['loginId']).username,datetime.now())
                        Log_register(request,activity)
                        content = "You Are Logged in"
                        phone = "+919539288508"
                        send_SMS(content,phone)

                        return redirect('index')
                    else:
                        messages.error(request,"Wrong Password")
                        return render(request,'login.html')
                else:
                    messages.error(request,"Username doestn't exist")
                    return render(request,'login.html')
            else:
                messages.error(request,'empty field')
        return render(request,'login.html')
    return render(request,'login.html')

def logout(request):
    print(request.session['loginId'])
    activity = "admin {} Logged out ".format(Admin.objects.get(id=request.session['loginId']).username)
    Log_register(request,activity)
    try:
        del request.session['loginId']
        messages.success(request,'See you later')
        return redirect('login')
    except KeyError:
        print('hi')
        messages.success(request,'See you later')
    return redirect('login')

def index(request):
   
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print("Current Time =", current_time)
    if 'loginId' in request.session:
        context = loginPerson(request)
        context['new_order_count'] = Order.objects.all().filter(order_status='pending').count()
        context['total_order'] = Order.objects.all().count()
        context['indexAct'] = True
        context['itemsCount'] = Product_Varients.objects.filter(status="Active").count()
        context['pendingItemCount'] = Product_Varients.objects.filter(status__in=["Pending","Suspended"]).count()
        today_min = dt.datetime.combine(dt.date.today(), dt.time.min)
        today_max = dt.datetime.combine(dt.date.today(), dt.time.max)
        context['saleCount'] = Order.objects.filter(order_date__range=(today_min,today_max)).count()
        return render(request,'index/index.html',context)
    else:
        return redirect('login')

    

def roles(request):
    if 'loginId' in request.session:
        
        if Admin.objects.get(id = request.session['loginId']).is_superuser == True:
            role_list = Admin_role.objects.all()
        else:
            
            role = [i.role.id for i in Admin.objects.filter(is_superuser = False)]
            role_list = Admin_role.objects.all().filter(id__in=role)
            
        if request.method == "POST":
            byRole = request.POST['byRole']
            byStatus = request.POST['byStatus']
            if byRole:
                role_list = role_list.filter(Q(role_name__icontains = byRole))
            if byStatus:
                role_list = role_list.filter(status = byStatus)
        if 'add roles' in has_permission(request)and'edit role' in has_permission(request)and'delete role' in has_permission(request)and'set permission' in has_permission(request) :
            context = loginPerson(request)
            context['permission'] = True
            context['role_list'] = Paginator(role_list,page_count).get_page(request.GET.get('page'))
            return render(request,'admins/role/roles.html',context)
        elif 'add roles'in has_permission(request) or'edit role'  in has_permission(request) or 'delete role' in has_permission(request) or 'set permission' in has_permission(request):
                context = loginPerson(request)
                context['permission'] = 1
                context['role_list'] = Paginator(role_list,page_count).get_page(request.GET.get('page'))
                return render(request,'admins/role/roles.html',context) 
       
        else:
            context = loginPerson(request)
            context['permission'] = False
            context['role_list'] = Paginator(role_list,page_count).get_page(request.GET.get('page'))
            return render(request,'admins/role/roles.html',context) 
    else:
        return redirect('login')

def add_roles(request):
    if 'loginId' in request.session:
        if 'add roles'in has_permission(request):
            print("yes")
            if request.method == "POST":
                form = Admin_roleForm(request.POST)
                if form.is_valid():
                    form.save()
                    now = datetime.now()
                    current_time = now.strftime("%H:%M:%S")
                    print(request.session['loginId'],"asdfsf")
                    
                    form1 = Log(user=Admin.objects.get(id=request.session['loginId']).username,activity="{} added role {} ".format(Admin.objects.get(id=request.session['loginId']).username,request.POST['role_name']),date=current_time)
                    form1.save()
                    messages.success(request,"Role added successfully")
                    return redirect('roles')
            context = loginPerson(request)
            context['form'] = Admin_roleForm() 
            context['permission'] = True    
        
            
            return render(request,'admins/role/add_roles.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('roles')
    else:
        return redirect('login')
    
def add_admin(request):
    if 'loginId' in request.session:
        if 'add admin' in has_permission(request):
            if request.method=="POST":
                form = AdminForm(request.POST)
                form.password = request.POST['password']
                print(form)
                if form.is_valid():
                    form.save()
                    activity = "admin {} added {} ".format(Admin.objects.get(id=request.session['loginId']).username,request.POST['username'])
                    Log_register(request,activity)
                    messages.success(request,"added successfully")
                    return redirect('admins')
                else:
                    messages.error(request,"can't create  user with the username already exist")
                    return redirect('admins')
          
            context = loginPerson(request)
            
            context['form'] = AdminForm

            return render(request,'admins/admin/add_admin.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('admins')
    else:
        return redirect('login')

def admins(request):
    if 'loginId' in request.session:
        obj = Admin.objects.all()

        if request.method == "POST":
            byName = request.POST['byName'] 
            byRole = request.POST['byRole'] 
            byStatus = request.POST['byStatus'] 
            if byName:
                obj =obj.filter(Q(username__icontains = byName))
            if byRole:
                obj =obj.filter(role = byRole)
            if byStatus:
                obj =obj.filter(status = byStatus)

        if 'change password' in has_permission(request) and 'view admin' in has_permission(request) and 'edit admin' in has_permission(request) and 'delete admin' in has_permission(request) and 'add admin' in has_permission(request):
            context = loginPerson(request)
            context['admin'] = Paginator(obj,page_count).get_page(request.GET.get('page'))
            context['roles'] = Admin_role.objects.all()
            context['permission1'] = True
            return render(request,'admins/admin/admins.html',context)
        elif 'change password' in has_permission(request) or 'view admin' in has_permission(request) or 'edit admin' in has_permission(request) or 'delete admin' in has_permission(request) or 'add admin' in has_permission(request):
            print(Admin.objects.get(id=request.session['loginId']).is_superuser)
            context = loginPerson(request)
            context['admin'] = Paginator(obj,page_count).get_page(request.GET.get('page'))
            context['roles'] = Admin_role.objects.all()
            context['permission1'] = 1
            return render(request,'admins/admin/admins.html',context)
        else:
            if Admin.objects.get(id=request.session['loginId']).is_superuser:

                pass
            else:
                obj = obj.exclude(is_superuser=True)
            
            context = loginPerson(request)
            context['superuser'] = Admin.objects.get(id=request.session['loginId']).is_superuser
            context['admin'] = Paginator(obj,page_count).get_page(request.GET.get('page'))
            context['roles'] = Admin_role.objects.all()
            context['permission1'] = False
            return render(request,'admins/admin/admins.html',context)
    else:
        return redirect('login')

def disable_admin(request):
    if 'loginId' in request.session:
        if 'disable admin' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Admin.objects.get(id=getById).status == 'active':
                        Admin.objects.filter(id=getById).update(status='suspended')
                        messages.warning(request,'Disabled')
                        return redirect('admins')
                    else:
                        Admin.objects.filter(id=getById).update(status='active')
                        messages.success(request,'Enabled')
                        return redirect('admins')
            return redirect('admins')
        else:
            messages.error(request,"access denied")
            return redirect('admins')
    else:
        return redirect('login')


     
def edit_role(request,pk):
    if 'loginId' in request.session:
        if 'edit role' in has_permission(request):
            if (request.method == "POST"):
                form = Admin_roleForm(request.POST,instance=Admin_role.objects.get(id=pk)) 
                if form.is_valid():
                    form.save()
                    activity = "admin {} edited {} ".format(Admin.objects.get(id=request.session['loginId']).username,Admin_role.objects.get(id=pk).role_name)
                    Log_register(request,activity)
                    return redirect('roles')
            form = Admin_roleForm(initial = {
                'role_name' : Admin_role.objects.get(id=pk).role_name,
                'status' : Admin_role.objects.get(id=pk).status,
            })

            context = loginPerson(request)
            context['form'] = form

            return render(request,'admins/role/edit_role.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('roles')
    else:
        return redirect('login')

def delete_role(request,pk):
    if 'loginId' in request.session:
        if 'delete role' in has_permission(request):
            form = Admin_role.objects.all().get(id=pk)
            role_list = Admin_role.objects.all()
            context = loginPerson(request)
            context['role_list'] = role_list
               
            if form:
                activity = "admin {} deleted role {}  ".format(Admin.objects.get(id=request.session['loginId']).username,Admin_role.objects.all().get(id=pk).role_name)
                Log_register(request,activity)
                form.delete()
                messages.success(request,"Role {} deleted successfully".format(form.role_name))
                return redirect('roles')
            return render(request,'admins/role/roles.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('roles')
    else:
        return redirect('login')

def set_permission(request,pk):
    if 'loginId' in request.session:
        if 'set permission' in has_permission(request):
            if(set_permissions.objects.all().filter(role=pk)):

                if request.method == "POST":
                    list1 = []
                    sub_path = request.POST.getlist('sub_perm[]')
                    main_path = request.POST.getlist('main_perm[]')
                    x = set_permissions.objects.all().filter(role=pk).delete()
                    # for i in range(len(sub_path),len(main_path)):
                    #     print(i,"hell")
                    #     x = set_permissions(role=Admin_role.objects.get(id=pk),sub_path=Sub_path.objects.get(id=sub_path[i]),main_path=Main_path.objects.get(id=main_path[i]))
                    #     x.save()
                    
                    for i in range(len(sub_path)):
                        print(i,"hell")
                        main_path = Sub_path.objects.get(id=sub_path[i]).Main_path_id.id
                        list1.append(main_path)
                        x = set_permissions(role=Admin_role.objects.get(id=pk),sub_path=Sub_path.objects.get(id=sub_path[i]),main_path=Main_path.objects.get(id=list1[i]))
                        x.save()
                    activity = "admin {} granded permission for role {}  ".format(Admin.objects.get(id=request.session['loginId']).username,Admin_role.objects.all().get(id=pk).role_name)
                    Log_register(request,activity)
                    messages.success(request,"{}'s permission granded successfully".format(Admin_role.objects.get(id=pk).role_name))
                    return redirect('roles')
            else:
                list1 = []
                if request.method == "POST":
                    sub_path = request.POST.getlist('sub_perm[]')
                    
                    for i in range(len(sub_path)):
                        main_path = Sub_path.objects.get(id=sub_path[i]).Main_path_id.id
                        list1.append(main_path)
                        x = set_permissions(role=Admin_role.objects.get(id=pk),sub_path=Sub_path.objects.get(id=sub_path[i]),main_path=Main_path.objects.get(id=list1[i]))
                        x.save()
                    activity = "admin {} granded permission for role {}  ".format(Admin.objects.get(id=request.session['loginId']).username,Admin_role.objects.all().get(id=pk).role_name)
                    Log_register(request,activity)    
                    messages.success(request,"{}'s permission granded successfully".format(Admin_role.objects.get(id=pk).role_name))
                    return redirect('roles')
            obj = Sub_path.objects.all()
            obj2 = set_permissions.objects.all().filter(role=pk).distinct()
            x = [i for i in obj2]
            y = [i.sub_path.path_name for i in x ]
            
            obj1 = Main_path.objects.all()
            
        
            
            context = loginPerson(request)    
            context['permission'] = set_permissions.objects.all()
        
        
            context['obj'] = obj
            context['obj1'] = obj1
            context['x'] = y
            context['z'] = [i.main_path.path_name for i in x ]
            return render(request,'admins/role/set_permission.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('roles')
    else:
        return redirect('login')

def add_main_path(request):
    if request.method == "POST":
        form = Main_pathForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('path')
    context = loginPerson(request)  
    context['form'] = Main_pathForm()
    return render(request,'admins/role/add_main_path.html',context)

def add_sub_path(request):
    if 'loginId' in request.session:
        if 'add path' in has_permission(request):
            if request.method == "POST":
                form = Sub_pathForm(request.POST)
                if form.is_valid():
                    form.save()
                    return redirect('path')
            context = loginPerson(request)    
            context['form'] = Sub_pathForm
    
            return render(request,'admins/role/add_sub_path.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('path')
    else:
        return redirect('login')

def path(request):
    if 'loginId' in request.session:
        obj = Sub_path.objects.all()
        context = loginPerson(request)
        context['obj'] = Paginator(obj,5).get_page(request.GET.get('page'))
        main_path = Main_path.objects.all() 
        context['main'] = Paginator(main_path,5).get_page(request.GET.get('page'))
        print(Paginator(main_path,10).get_page(request.GET.get('page')))
    
        return render(request,'admins/role/paths.html',context)
    else:
        return redirect('login')
    
#_____region_____
def Region(request):
    if 'loginId' in request.session:
        if 'add region' in has_permission(request) and 'enable region' in has_permission(request) and 'add/remove pincode' in has_permission(request):
            data = RegionTable.objects.all()
            context = loginPerson(request)
            allRegions = RegionTable.objects.all()
            context['Region'] = Paginator(allRegions,10).get_page(request.GET.get('page'))
            context['Locations'] = True
            context['permission'] = True
            if request.method=="POST":
                byName = request.POST['byName']
                byScode = request.POST['byScode']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allRegions = allRegions.filter(Q(Region_name__icontains=byName))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byScode:
                    allRegions = allRegions.filter(Q(Short_code__icontains=byScode))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byStatus:
                    allRegions = allRegions.filter(status=byStatus)
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
            return render(request,'location/region/region.html',context)
        elif 'add region' in has_permission(request) or 'enable region' in has_permission(request) or 'add/remove pincode' in has_permission(request):
            data = RegionTable.objects.all()
            context = loginPerson(request)
            allRegions = RegionTable.objects.all()
            context['Region'] = Paginator(allRegions,10).get_page(request.GET.get('page'))
            context['Locations'] = True
            context['permission'] = 1
            if request.method=="POST":
                byName = request.POST['byName']
                byScode = request.POST['byScode']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allRegions = allRegions.filter(Q(Region_name__icontains=byName))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byScode:
                    allRegions = allRegions.filter(Q(Short_code__icontains=byScode))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byStatus:
                    allRegions = allRegions.filter(status=byStatus)
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
            return render(request,'location/region/region.html',context)
        else:
            data = RegionTable.objects.all()
            context = loginPerson(request)
            allRegions = RegionTable.objects.all()
            context['Region'] = Paginator(allRegions,10).get_page(request.GET.get('page'))
            context['Locations'] = True
            context['permission'] = False
            if request.method=="POST":
                byName = request.POST['byName']
                byScode = request.POST['byScode']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allRegions = allRegions.filter(Q(Region_name__icontains=byName))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byScode:
                    allRegions = allRegions.filter(Q(Short_code__icontains=byScode))
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
                if byStatus:
                    allRegions = allRegions.filter(status=byStatus)
                    context['Region'] = Paginator(allRegions,page_count).get_page(request.GET.get('page'))
            return render(request,'location/region/region.html',context)
    else:
        return redirect('login')

def addRegion(request):
    if 'loginId' in request.session:
        if 'add region' in has_permission(request):
            if request.method == "POST":
                regionFormData = regionForm(request.POST)
                
                if regionFormData.is_valid():
                    print('here')
                    regionFormData.save()
                    messages.success(request,'{} successfully added'.format(regionFormData.cleaned_data['Region_name']))
                    return redirect('Region')
                else:
                    pass
            context = loginPerson(request)
            context['form'] = regionForm()
            context['Locations'] = 'True'
            return render(request,'location/region/addregion.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Region')
    else:
        return redirect('login')

def disableRegion(request):
    if 'loginId' in request.session:
        if 'enable region' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if RegionTable.objects.get(id=getById).status == 'Active':
                        RegionTable.objects.filter(id=getById).update(status='Suspended')
                        messages.warning(request,'Disabled')
                        return redirect('Region')
                    else:
                        RegionTable.objects.filter(id=getById).update(status='Active')
                        messages.success(request,'Enabled')
                        return redirect('Region')
            return redirect('Region')
        else:
            messages.error(request,"access denied")
            return redirect('Region')
    else:
        return redirect('login')

def editRegion(request,pk):
    if 'loginId' in request.session:
        if 'edit location' in has_permission(request):
            if request.method=="POST":
                regionFormData = regionForm(request.POST,instance=RegionTable.objects.get(id=pk))
                if regionFormData.is_valid():
                    regionFormData.save()
                    messages.success(request,'{} is successfully updated'.format(regionFormData.cleaned_data['Region_name']))
                    return redirect('Region')
            context = loginPerson(request)
            context['form'] = regionForm(initial={
                'Region_name':RegionTable.objects.get(id=pk).Region_name,
                'Short_code':RegionTable.objects.get(id=pk).Short_code,
                'status':RegionTable.objects.get(id=pk).status,               
            })
            context['regionid'] = pk
            context['Locations'] = True
            context['loginPesron'] = Admin.objects.get(id=request.session['loginId'])
            return render(request,'location/region/editregion.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Region')
    else:
        return redirect('login')

def pincode(request,pk):
    if 'loginId' in request.session:
        if 'add/remove pincode' in has_permission(request):
            context = loginPerson(request)
            context['form'] = PincodeForm(initial={
                    'Owner':pk,
                })
            context['pincode'] = Paginator(PincodeTable.objects.all().filter(Owner=pk),2).get_page(request.GET.get('page'))
            context['ownerid'] = pk
            context['Locations'] = True
            context['loginPesron'] = Admin.objects.get(id=request.session['loginId'])
            return render(request,'location/region/addorremovepin.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Region')
    else:
        return redirect('login')

def addPincode(request):
    if 'loginId' in request.session:
        if request.method=="POST":
            PincodeFormData=PincodeForm(request.POST)
            if PincodeFormData.is_valid():
                PincodeFormData.save()
                messages.success(request,'pincode of {} is added'.format(PincodeFormData.cleaned_data['Owner']))
                return redirect('Region')
            else:
                messages.warning(request,'pincode of {} is already exist'.format(PincodeFormData.cleaned_data['Owner']))
                return redirect('Region')
        return redirect('Region')
    else:
        return redirect('login')

def disablePincode(request):
    if 'loginId' in request.session:
        if request.method == "POST":
            getById=request.POST['getById']
            if getById:
                data = PincodeTable.objects.filter(id=getById)
                if data:
                    data.delete()
                    messages.warning(request,'Removed')
                    return redirect('Region')
                else:
                    messages.error(request,'something went wrong!!')
                    return redirect('Region')
        return redirect('Region')
    else:
        return redirect('login')

def edit_admins(request,pk):
    
    if 'loginId' in request.session:
        if 'edit admin' in has_permission(request):
            form = Admin.objects.get(id=pk)
            role = Admin_role.objects.all()
            region = RegionTable.objects.all()
            
            if (request.method == "POST"):
                form = AdminForm(request.POST,instance = Admin.objects.get(id=pk))
                if form.is_valid():
                    form.save() 
                    activity = "{} {} editted {} {}  ".format(Admin_role.objects.get(id=Admin.objects.get(id=request.session['loginId']).role.id).role_name,Admin.objects.get(id=request.session['loginId']).username,Admin_role.objects.get(id=Admin.objects.get(id=pk).role.id).role_name, Admin.objects.get(id=pk).username)
                    Log_register(request,activity)
                    form = Admin.objects.get(id=pk)
                    messages.success(request,"{} updated successfully".format(form.username))
                    return redirect('admins')
            form = AdminForm(initial = {
                'code' :Admin.objects.get(id=pk).code,
                'role' :Admin.objects.get(id=pk).role,
                'name' :Admin.objects.get(id=pk).name,

                'email' :Admin.objects.get(id=pk).email,

                'phone' :Admin.objects.get(id=pk).phone,

                'username' :Admin.objects.get(id=pk).username,
                'password' :Admin.objects.get(id=pk).password,

                'status' :Admin.objects.get(id=pk).status,
                'region' :Admin.objects.get(id=pk).region,

            })
            context = loginPerson(request) 
            context['form'] = form
            context['role'] = role
            context['region'] = region
            
            return render(request,'admins/admin/edit_admins.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('admins')
    else:
        return redirect('login')
        

def view_admins(request,pk):
    if 'loginId' in request.session:
        if 'view admin' in has_permission(request):
            form = Admin.objects.get(id=pk)
            context = loginPerson(request) 
            context['form'] = form
        
            return render(request,'admins/admin/view_admins.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('admins')
    else:
        return redirect('login')


def change_password(request,pk):
    if 'loginId' in request.session:
        if 'change password' in has_permission(request):
            form =  Admin.objects.all().get(id=pk)
            if request.method == "POST":
                
                new_password = request.POST['new_password']
                new_password_confirmation = request.POST['new_password_confirmation']
                if new_password == new_password_confirmation:
                    form.password = request.POST['new_password']
                    if form.password:
                        form.save()
                        messages.success(request,"password changed successfully")
                        return redirect('admins')
                else:
                    messages.error(request,"password not match")
                
            context = loginPerson(request) 
            context['form'] = form
        
            return render(request,'admins/admin/change_password.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('admins')
    else:
        return redirect('login')

#____banner_____
def Banner(request):
    if 'loginId' in request.session:
        if 'add banners' in has_permission(request) and 'edit banners' in has_permission(request) and 'disable banners' in has_permission(request):
            global page_count
            context = loginPerson(request)
            allBanners = BannerTable.objects.all()
            context['BannerAct'] = True
            context['permission'] = True
            if request.method=="POST":
                byName = request.POST['byName']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allBanners = allBanners.filter(Q(Banner_name__icontains=byName))
                if byStatus:
                    allBanners = allBanners.filter(status=byStatus)
            
            context['Banners'] = Paginator(allBanners.order_by("Banner_Display_Order"),page_count).get_page(request.GET.get('page'))
            return render(request,'banners/banners.html',context)
        elif 'add banners' in has_permission(request) or 'edit banners' in has_permission(request) or 'disable banners' in has_permission(request):
            
            context = loginPerson(request)
            allBanners = BannerTable.objects.all()
            context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
            context['BannerAct'] = True
            context['permission'] = 1
            if request.method=="POST":
                byName = request.POST['byName']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allBanners = allBanners.filter(Q(Banner_name__icontains=byName))
                    context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
                if byStatus:
                    allBanners = allBanners.filter(status=byStatus)
                    context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
            return render(request,'banners/banners.html',context)
        else:
            
            context = loginPerson(request)
            allBanners = BannerTable.objects.all()
            context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
            context['BannerAct'] = True
            context['permission'] = False
            if request.method=="POST":
                byName = request.POST['byName']
                byStatus = request.POST.get('byStatus')
                if byName:
                    allBanners = allBanners.filter(Q(Banner_name__icontains=byName))
                    context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
                if byStatus:
                    allBanners = allBanners.filter(status=byStatus)
                    context['Banners'] = Paginator(allBanners,page_count).get_page(request.GET.get('page'))
            return render(request,'banners/banners.html',context)
    else:
        return redirect('login')

#add banner
def addBanner(request):
    if 'loginId' in request.session:
        if 'add banners' in has_permission(request):
            if request.method=="POST":
                BannerFormData = BannerForm(request.POST,request.FILES)
                print('im not')
                if BannerFormData.is_valid():
                    print('valid')
                    BannerFormData.save()
                    messages.success(request,'banner {} successfully added'.format(BannerFormData.cleaned_data['Banner_name']))
                    return redirect('Banner')
                else:
                    pass
            context = loginPerson(request)
            context['form'] = BannerForm()
            context['BannerAct'] = True
            return render(request,'banners/addbanners.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Banner')
    else:
        return redirect('login')

#disable banner
def disableBanner(request):
    if 'loginId' in request.session:
        if 'disable banners' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if BannerTable.objects.get(id=getById).status == 'Active':
                        BannerTable.objects.filter(id=getById).update(status='Suspended')
                        messages.warning(request,'Disabled')
                        return redirect('Banner')
                    else:
                        BannerTable.objects.filter(id=getById).update(status='Active')
                        messages.success(request,'Enabled')
                        return redirect('Banner')
            return redirect('Banner')
        else:
            messages.error(request,"access denied")
            return redirect('Banner')
    else:
        return redirect('login')

#edit banner
def editBanner(request,pk):
    if 'loginId' in request.session:
        if 'edit banners' in has_permission(request):
            if request.method=="POST":
                BannerFormData = BannerForm(request.POST,request.FILES,instance=BannerTable.objects.get(id=pk))
                if BannerFormData.is_valid():
                    BannerFormData.save()
                    messages.success(request,'{} is updated successfully'.format(BannerFormData.cleaned_data['Banner_name']))
                    return redirect('Banner')
                else:
                    messages.warning(request,'Something went wrong')
                    return redirect('Banner')
            context = loginPerson(request)
            context['form'] = BannerForm(initial = {
                'Banner_name':BannerTable.objects.get(id=pk).Banner_name,
                'Banner_URL':BannerTable.objects.get(id=pk).Banner_URL,
                'Banner_Display_Order':BannerTable.objects.get(id=pk).Banner_Display_Order,
                'Banner_image':BannerTable.objects.get(id=pk).Banner_image,
                'App_Banner_image':BannerTable.objects.get(id=pk).App_Banner_image,
                'status':BannerTable.objects.get(id=pk).status,
            })
            context['bannerid'] = pk
            context['BannerAct'] = True
            return render(request,'banners/editbanner.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Banner')
    else:
        return redirect('login')


def offers(request):
    if 'loginId' in request.session:
        offer = Offers.objects.all()
        today = date.today()
        context = loginPerson(request)
        if request.method == "POST":
            byOffer = request.POST['byOffer']
            byStatus = request.POST['byStatus']
            if byOffer:
                offer = offer.filter(Q(offer_name__icontains = byOffer))
            if byStatus:
                offer = offer.filter(status=byStatus)
        context['offer'] = Paginator(offer,page_count).get_page(request.GET.get('page'))
        context['today'] = today
    
        return render(request,'offers/offers.html',context)
    else:
        return redirect('login')

def add_offers(request):
    if 'loginId' in request.session:
        if 'add offers' in has_permission(request):
            if(request.method == "POST"):
                form = offersForm(request.POST,request.FILES)
                if(form.is_valid()):
                    form.save()
                    return redirect('offers')
            context = loginPerson(request)
            context['form'] = offersForm
    
            return render(request,'offers/add_offers.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('offers')
    else:
        return redirect('login')

def edit_offers(request,pk):
    if 'loginId' in request.session:
        
        if 'edit offers' in has_permission(request):
            
            if(request.method == "POST"):
                form = offersForm(request.POST,request.FILES,instance=Offers.objects.get(id=pk))
                if form.is_valid():
                    form.save()
                    messages.success(request,"{} updated successfully")
                    return redirect('offers')
                else:
                    messages.error(request,"{} update failed")
                    return redirect('offers')

            form = offersForm(initial={
                'offer_name':Offers.objects.get(id=pk).offer_name,
                
                
                'image':Offers.objects.get(id=pk).image,
                'App_banar_image':Offers.objects.get(id=pk).App_banar_image,
                'status':Offers.objects.get(id=pk).status,
            
            })

            context = loginPerson(request)
            context['form'] = form
        
            return render(request,'offers/edit_offers.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('offers') 
    else:
        return redirect('login')

def delete_offers(request):
    if 'loginId' in request.session:
        if 'disable offers' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Offers.objects.get(id=getById).status:
                        Offers.objects.filter(id=getById).update(status=False)
                        messages.warning(request,'Disabled')
                        return redirect('offers')
                    else:
                        Offers.objects.filter(id=getById).update(status=True)
                        messages.success(request,'Enabled')
                        return redirect('offers')
            return redirect('offers')
        else:
            messages.error(request,"access denied")
            return redirect('offers')
    else:
        return redirect('login')


def add_product_to_offers(request):
    if 'loginId' in request.session:
        offer_data = Offers.objects.get(id = request.GET.get('offer',None)) 

        if request.method == "POST":
            string_data = ""
            print(offer_data)
            for i in offer_data.product_varients_set.all():
                i.Product_Offers.remove(offer_data)
            product_list = request.POST.getlist('items')
            for item in product_list:
                Item_data = Product_Varients.objects.get(id = item)
                Item_data.Product_Offers.add(offer_data)
                Item_data.save()
                print(Item_data.product_set.first().Name)
                string_data = string_data  + " "+str(Item_data.product_set.first().Name) + "-" + str(Item_data.Varient_Values)+","
            messages.info(request,'Products of {} is applied to the {} offer'.format(string_data,offer_data))
            return redirect('offers')
        item_list = [i.id for i in offer_data.product_varients_set.all()]
        print(item_list)
        context = loginPerson(request)
        context['offer_data'] = offer_data
        context['item_list'] = item_list
        context['offer_act'] = True
        context['Items'] = Product_Varients.objects.filter(status = "Active").order_by('-id')
        print(context['Items'])
        return render(request,'offers/add_product_to_offer.html',context) 
    else:
        return redirect('login')




#_____category_____
def CategoryView(request):
    allCategory = Category_table.objects.all()
    if request.method=="POST":
        byName = request.POST['byName']
        byParents = request.POST['byParents']
        byStatus = request.POST.get('byStatus')
        if byName:
            allCategory = allCategory.filter(Q(Category_Name__icontains=byName))
        if byParents:
            allCategory = allCategory.filter(Parent__Category_Name=byParents)
        if byStatus:
            allCategory = allCategory.filter(status=byStatus)
        
    context = loginPerson(request)
    context['Inventory'] = True
    context['Categories'] = Paginator(allCategory.order_by('Display_Order'),page_count).get_page(request.GET.get('page'))
    return render(request,'Inventory/category/category.html',context)

def addCategory(request):
    if 'loginId' in request.session:
        if 'add catagory' in has_permission(request):
            if request.method == "POST":
                CategoryFormData = CategoryForm(request.POST,request.FILES)
                if CategoryFormData.is_valid():
                    CategoryFormData.save()
                    messages.success(request,'{} is successfully added'.format(CategoryFormData.cleaned_data['Category_Name']))
                    return redirect('CategoryView')
                else:
                    messages.warning(request,'somethinf went wrong!!')
                    return redirect('CategoryView')
            context = loginPerson(request)
            context['form'] = CategoryForm()
            context['Inventory'] = True
            Parent = [i for i in Category_table.objects.filter(Parent=None)]
            SubParent=[i for i in Category_table.objects.filter(Parent__id__in=[i.id for i in Category_table.objects.filter(Parent=None)])]
            Parent.extend(SubParent)
            context['categories'] = Parent
            return render(request,'Inventory/category/addCategory.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('CategoryView')
    else:
        return redirect('login')

def disableCategory(request):
    if 'loginId' in request.session:
        if 'disable catagory' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Category_table.objects.get(id=getById).status == 'Active':
                        Category_table.objects.filter(id=getById).update(status='Suspended')
                        messages.warning(request,'Disabled')
                        return redirect('CategoryView')
                    else:
                        Category_table.objects.filter(id=getById).update(status='Active')
                        messages.success(request,'Enabled')
                        return redirect('CategoryView')
                return redirect('CategoryView')
        else:
            messages.error(request,"access denied")
            return redirect('CategoryView')
    else:
        return redirect('login')

def editCategory(request,pk):
    if 'loginId' in request.session:
        if 'edit catagory' in has_permission(request):
            if request.method == "POST":
                CategoryFormData = CategoryForm(request.POST,request.FILES,instance=Category_table.objects.get(id=pk))
                if CategoryFormData.is_valid():
                    CategoryFormData.save()
                    messages.success(request,'{} is successfully updated'.format(CategoryFormData.cleaned_data['Category_Name']))
                    return redirect('CategoryView')
                else:
                    messages.warning(request,'something went wrong')
                    return redirect('CategoryView')
            context = loginPerson(request)
            context['form'] = CategoryForm(initial={
                'Category_Name':Category_table.objects.get(id=pk).Category_Name,
                'Display_Order':Category_table.objects.get(id=pk).Display_Order,
                'Category_img':Category_table.objects.get(id=pk).Category_img,
                'Parent':Category_table.objects.get(id=pk).Parent,
                'status':Category_table.objects.get(id=pk).status,
            })
            context['CatId'] = pk
            context['Inventory'] = True
            Parent = [i for i in Category_table.objects.filter(Parent=None)]
            SubParent=[i for i in Category_table.objects.filter(Parent__id__in=[i.id for i in Category_table.objects.filter(Parent=None)])]
            Parent.extend(SubParent)
            context['categories'] = Parent
            return render(request,'Inventory/category/editCategory.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('CategoryView')
    else:
        return redirect('login')

def Varient(request):
    context = loginPerson(request)
    allVarient = Varient_Type.objects.all()
    context['Inventory'] = True
    if request.method=="POST":
        byVarient = request.POST['byVarient']
        byValues = request.POST['byValues']
        byStatus = request.POST.get('byStatus')
        if byVarient:
            allVarient = allVarient.filter(Q(Varient_Name__icontains=byVarient))
        if byValues:
            allVarient = allVarient.filter(Q(varient_values__Varient_Values__icontains=byValues)).distinct()
        if byStatus:
            allVarient = allVarient.filter(status=byStatus)
            
    context['Varients'] = Paginator(allVarient.order_by("Display_Order"),page_count).get_page(request.GET.get('page'))
    return render(request,'Inventory/varients/varientsvalues.html',context)

def addVarient(request):
    if 'loginId' in request.session:
        if 'add varient' in has_permission(request):
            if request.method == "POST":
                Varient_TypeFormData = Varient_TypeForm(request.POST)
                if Varient_TypeFormData.is_valid():
                    Varient_TypeFormData.save()
                    messages.success(request,'{} is added successfully'.format(Varient_TypeFormData.cleaned_data['Varient_Name']))
                    return redirect('Varient')
                else:
                    messages.warning(request,'something went wrong!!')
                    return redirect('Varient')
            context = loginPerson(request)
            context['form'] = Varient_TypeForm()
            context['Inventory'] = True
            return render(request,'Inventory/varients/addVarient.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Varient')
    else:
        return redirect('login')

def disableVarient(request):
    if 'loginId' in request.session:
        if 'disable varient' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Varient_Type.objects.get(id=getById).status == 'Active':
                        Varient_Type.objects.filter(id=getById).update(status='Suspended')
                        messages.warning(request,'Disabled')
                        return redirect('Varient')
                    else:
                        Varient_Type.objects.filter(id=getById).update(status='Active')
                        messages.success(request,'Enabled')
                        return redirect('Varient')
            return redirect('Varient')
        else:
            messages.error(request,"access denied")
            return redirect('Varient')
    else:
        return redirect('login')

def addValues(request,pk):
    if 'loginId' in request.session:
        if 'add varient values' in has_permission(request):
            if request.method=="POST":
                Varient_ValuesFormData = Varient_ValuesForm(request.POST)
                if Varient_ValuesFormData.is_valid():
                    Varient_ValuesFormData.save()
                    messages.success(request,'{} is succesfuly added'.format(Varient_ValuesFormData.cleaned_data['Varient_Values']))
                    return redirect('Varient')
                else:
                    messages.warning(request,'something went wrong!!')
                    return redirect('Varient')
            context = loginPerson(request)
            context['form'] = Varient_ValuesForm(initial={
                'Varient_Type':pk
            })
            context['valueId'] = pk
            context['Inventory'] = True
            context['Varient']=Varient_Type.objects.get(id=pk)
            return render(request,'Inventory/varients/addvarientValues.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Varient')
    else:
        return redirect('login')

def ViewValues(request):
    id_Value = request.GET.get('varient', None)
    print(id_Value)
    items = Varient_Values.objects.all().filter(Varient_Type=id_Value).order_by('Display_Order')
    itemList = [i.Varient_Values for i in items if i.status=='Active']
    data={
        'item':itemList
    }
    return JsonResponse(data)

def editVarient(request,pk):
    if 'loginId' in request.session:
        if 'edit varient' in has_permission(request):
            if request.method == "POST":
                Varient_TypeFormData = Varient_TypeForm(request.POST,instance=Varient_Type.objects.get(id=pk))
                if Varient_TypeFormData.is_valid():
                    Varient_TypeFormData.save()
                    messages.success(request,'{} is updated'.format(Varient_TypeFormData.cleaned_data['Varient_Name']))
                    return redirect('Varient')
                else:
                    messages.warning(request,'something went wrong!!')
                    return redirect('Varient')
            context = loginPerson(request)
            context['form'] = Varient_TypeForm(initial={
                'Varient_Name':Varient_Type.objects.get(id=pk).Varient_Name,
                'Display_Order':Varient_Type.objects.get(id=pk).Display_Order,
                'status':Varient_Type.objects.get(id=pk).status,
            })
            context['Inventory'] = True
            context['valueId'] = pk
            context['VarientsValues'] = Varient_Values.objects.filter(Varient_Type=pk).order_by('Display_Order')
            return render(request,'Inventory/varients/editVarients.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('Varient')
    else:
        return redirect('login')

def disableVarientValues(request):
    if 'loginId' in request.session:
        if request.method == "POST":
            getById=request.POST['getById']
            if getById:
                varient = Varient_Values.objects.get(id=getById).Varient_Type.id
                if Varient_Values.objects.get(id=getById).status == 'Active':
                    Varient_Values.objects.filter(id=getById).update(status='Suspended')
                    messages.warning(request,'Disabled')
                    return redirect('editVarient',pk=varient)
                else:
                    Varient_Values.objects.filter(id=getById).update(status='Active')
                    messages.success(request,'Enabled')
                    return redirect('editVarient',pk=varient)
        return redirect('Varient')
    else:
        return redirect('login')
#____sales report_____
def SalesReport(request):
    if 'loginId' in request.session:
        global page_count
        context = loginPerson(request)
        context['salesReport'] = Paginator(SalesReportTable.objects.all(),page_count).get_page(request.GET.get('page'))
        context['Report'] = True
        return render(request,'SalesReport/salesReport.html',context)
    else:
        return redirect('login')

def customers(request):
    customer_obj = Customer.objects.all().order_by('-registered_date')
    context = loginPerson(request)
    if request.method == "POST":
        byName = request.POST['filter_name']
        filter_phone = request.POST['filter_phone']
        filter_from_date = request.POST['filter_from_date']
        filter_to_date = request.POST['filter_to_date']
        byStatus = request.POST['byStatus']
        if byName:
            customer_obj = customer_obj.filter(Q(name__icontains = byName))
        if filter_phone:
            customer_obj = customer_obj.filter(phone = filter_phone)
        if byStatus:
            customer_obj = customer_obj.filter(status = byStatus)
        if filter_from_date or filter_to_date:
            if filter_from_date:
                today = date.today().strftime("%Y-%m-%d")
                customer_obj = customer_obj.filter(registered_date__range=(filter_from_date,today))
            if filter_to_date:
                fromDate = Customer.objects.all().order_by('registered_date').first().registered_date
                customer_obj = customer_obj.filter(registered_date__range=(fromDate,filter_to_date))
            if filter_from_date and filter_to_date:
                customer_obj = customer_obj.filter(registered_date__range=(filter_from_date,filter_to_date))

    context['customer_obj'] = Paginator(customer_obj,page_count).get_page(request.GET.get('page'))
    return render(request,'customers/customers.html',context)
 

def orders(request):
    if 'loginId' in request.session:
        if 'orders' in has_permission(request):
            orderObj = Order.objects.all().order_by('-order_date')
            if(request.method == "POST"):
                filter_or_not = request.POST.get('filter_or_not')
                if filter_or_not is None:
                    value_id = request.POST.get("value_id")
                    p_status = request.POST.get("payment_status")
                    o_status = request.POST.get("order_status")
                    print(o_status)
                    if value_id:
                        order_obj = Order.objects.get(id=value_id)
                        cust_mail = [i.user.mail for i in Order.objects.filter(id=value_id)]
                        order_id = Order.objects.get(id=value_id).order_id_m
                        if p_status == "" or o_status == "":
                            messages.error(request,"please choose anything")
                            return redirect('orders')
                        else:
                            #TODO:sree sms content
                            if o_status == "processing":
                                content = PROCESSED_CONTENT
                                send_SMS(content,"+919947242749")
                            if o_status == "shipped":
                                content = SHIPPED_CONTENT
                                send_SMS(content,"+919947242749")
                            if o_status == "out for delivery":
                                content = ORDER_OUT_FOR_DELIVERY
                                send_SMS(content,"+919947242749")
                            if o_status == "delivered":
                                content = ORDER_DEIVERD
                                send_SMS(content,"+919947242749")

                            subject = 'Delivery Update Momooz'
                            message = 'your Order with order id :'+order_id+' is '+o_status
                            from_email = 'srees0732@gmail.com'
                            send_mail(
                                subject,
                                message,
                                from_email,
                                cust_mail,

                            )
                            order_obj.order_status = o_status
                            order_obj.payment_status = p_status
                            order_obj.save()
                            messages.success(request,"updated")
                            return redirect('orders')
                else:
                    customer = request.POST.get('customer')
                    order_status = request.POST.get('order_status')
                    payment_status = request.POST.get('payment_status')
                    dateFrom = request.POST.get('dateFrom')
                    dateTo = request.POST.get('dateTo')
                    if customer:
                        orderObj = orderObj.filter(Q(user__name__icontains = customer))
                    if order_status:
                        orderObj = orderObj.filter(order_status = order_status)
                    if payment_status:
                        orderObj = orderObj.filter(payment_status = payment_status)
                    if dateTo or dateFrom:
                        if dateFrom:
                            today = date.today().strftime("%Y-%m-%d")
                            orderObj = orderObj.filter(order_date__range=(dateFrom,today))
                        if dateTo:
                            From = Order.objects.all().order_by('order_date').first().order_date
                            orderObj = orderObj.filter(order_date__range=(From,dateTo))
                        if dateTo and dateFrom:
                            orderObj = orderObj.filter(order_date__range=(dateFrom,dateTo))
                            
            order_obj = Order_details.objects.all()
            context = loginPerson(request)
            context['order_details'] = order_obj
            context['permission'] = True
            context['order'] = Paginator(orderObj,page_count).get_page(request.GET.get('page'))
            return render(request,'orders/orders.html',context)
        else:
            context = loginPerson(request)
            context['permission'] = False
            return render(request,'orders/orders.html',context)         
    else:
        return redirect('login')


def new_orders(request):
    orderObj = Order.objects.all().filter(order_status="pending")
    context = loginPerson(request)
    if(request.method == "POST"):
        filter_or_not = request.POST.get('filter_or_not')
        if filter_or_not is None:
            value_id = request.POST.get("value_id")
            p_status = request.POST.get("payment_status")
            o_status = request.POST.get("order_status")
            if value_id:
                order_obj = Order.objects.get(id=value_id)
                cust_mail = [i.user.mail for i in Order.objects.filter(id=value_id)]
                order_id = Order.objects.get(id=value_id).order_id_m
                if p_status == "" or o_status == "":
                    messages.error(request,"please choose anything")
                    return redirect('new_orders')
                else:
                    #TODO:sree sms content
                    if o_status == "processing":
                        content = PROCESSED_CONTENT
                        send_SMS(content,"+919947242749")
                    if o_status == "shipped":
                        content = SHIPPED_CONTENT
                        send_SMS(content,"+919947242749")
                    if o_status == "out for delivery":
                        content = ORDER_OUT_FOR_DELIVERY
                        send_SMS(content,"+919947242749")
                    if o_status == "delivered":
                        content = ORDER_DEIVERD
                        send_SMS(content,"+919947242749")

                    subject = 'Delivery Update Momooz'
                    message = 'your Order with order id :'+order_id+' is '+o_status
                    from_email = 'srees0732@gmail.com'
                    send_mail(
                        subject,
                        message,
                        from_email,
                        cust_mail,

                    )
                    order_obj.order_status = o_status
                    order_obj.payment_status = p_status
                    order_obj.save()
                    messages.success(request,"updated")
                    return redirect('new_orders')
        else:
            customer = request.POST.get('customer')
            order_status = request.POST.get('order_status')
            payment_status = request.POST.get('payment_status')
            dateFrom = request.POST.get('dateFrom')
            dateTo = request.POST.get('dateTo')
            if customer:
                orderObj = orderObj.filter(Q(user__name__icontains = customer))
            if order_status:
                orderObj = orderObj.filter(order_status = order_status)
            if payment_status:
                orderObj = orderObj.filter(payment_status = payment_status)
            if dateTo or dateFrom:
                if dateFrom:
                    today = date.today().strftime("%Y-%m-%d")
                    orderObj = orderObj.filter(order_date__range=(dateFrom,today))
                if dateTo:
                    From = Order.objects.all().order_by('order_date').first().order_date
                    orderObj = orderObj.filter(order_date__range=(From,dateTo))
                if dateTo and dateFrom:
                    orderObj = orderObj.filter(order_date__range=(dateFrom,dateTo))
    context['indexAct'] = True              
    context['order_details'] = Order_details.objects.all()
    context['order'] = Paginator(orderObj.order_by("-id"),page_count).get_page(request.GET.get('page'))
    return render(request,'orders/new_orders.html',context)

def coupons(request):
    if 'loginId' in request.session:
        coupon_obj = Coupon_code.objects.all()
        context = loginPerson(request)
        context['coupon_obj'] = coupon_obj
    
        
        today = date.today()
        
        context['today'] = today
        return render(request,'coupons/coupons.html',context)
    else:
        return redirect('login')

def add_coupon(request):
    if 'loginId' in request.session:
        if 'add coupon' in has_permission(request):
            if request.method == "POST":
                form = Coupon_codeForm(request.POST)
                if form.is_valid():
                    form.save()
                    messages.success(request,"coupon added successfully")
                    return redirect("coupons")
                
            form = Coupon_codeForm()
            context = loginPerson(request)
            context['form'] = form
        
            return render(request,'coupons/add_coupon.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('coupons')
    else:
        return redirect('login')

def edit_coupons(request,pk):
    if 'loginId' in request.session:
        if 'edit coupon' in has_permission(request):
            if request.method == "POST":
                form = Coupon_codeForm(request.POST,instance = Coupon_code.objects.get(id=pk))
                print(form)
                if form.is_valid():
                    form.save()
                    messages.success(request,"edited successfully")
                    return redirect('coupons')
            
            context = loginPerson(request)
            context['form'] = Coupon_codeForm(initial = {
                'coupon_code' : Coupon_code.objects.get(id=pk).coupon_code,
                'from_date' : Coupon_code.objects.get(id=pk).from_date,
                'to_date' : Coupon_code.objects.get(id=pk).from_date,
                'type' : Coupon_code.objects.get(id=pk).type,
                'discount' : Coupon_code.objects.get(id=pk).discount,
                'user_count' : Coupon_code.objects.get(id=pk).user_count,
                'min_amount' : Coupon_code.objects.get(id=pk).min_amount,
                'status' : Coupon_code.objects.get(id=pk).status,
            })

            
            
            return render(request,'coupons/edit_coupons.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('coupons')
    else:
        return redirect('login')

def delete_coupons(request,pk):
    if 'loginId' in request.session:
        if 'delete coupon' in has_permission(request):
            form = Coupon_code.objects.get(id=pk)
            if form:
                form.delete()
                messages.success(request,"deleted successfully")
                return redirect('coupons')
            return render(request,"coupons/coupons.html")
        else:
            messages.error(request,"access denied")
            return redirect('coupons')
    else:
        return redirect('login')

def coupon_code_users(request):
    context = loginPerson(request)
    return render(request,'coupons/coupon_code_users.html',context)

def brands(request):
    if 'loginId' in request.session:
        brand_obj = Brand.objects.all()
        context = loginPerson(request)
        if request.method == "POST":
            byBrand = request.POST['byBrand']
            byStatus = request.POST['byStatus']
            print(byBrand,byStatus)
            if byBrand:
                brand_obj=brand_obj.filter(Q(brand__icontains = byBrand))
            if byStatus:
                brand_obj=brand_obj.filter(status = byStatus)
        context['brand'] = Paginator(brand_obj.order_by('-id'),page_count).get_page(request.GET.get('page'))
        context['brand_act'] = True
        return render(request,'brand/brands.html',context)
    else:
        return redirect('login')

def add_brand(request):
    if 'loginId' in request.session:
        if 'add brand' in has_permission(request):
            form = BrandForm(request.POST,request.FILES)
            if form.is_valid():
                form.save()
                messages.success(request,"brand added successfully")
                return redirect('brands')
            context = loginPerson(request)
            context['form'] = BrandForm()
        
            return render(request,'brand/add_brand.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('brands')
    else:
        return redirect('login')

def delete_brand(request):
    
    if 'loginId' in request.session:
        if 'disable brand' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Brand.objects.get(id=getById).status:
                        Brand.objects.filter(id=getById).update(status=False)
                        messages.warning(request,'Disabled')
                        return redirect('brands')
                    else:
                        Brand.objects.filter(id=getById).update(status=True)
                        messages.success(request,'Enabled')
                        return redirect('brands')
            return redirect('brands')
        else:
            messages.error(request,"access denied")
            return redirect('brands')
    else:
        return redirect('login')

def edit_brand(request,pk):
    if 'loginId' in request.session:
        if 'edit brand' in has_permission(request):
            form = BrandForm(request.POST,request.FILES,instance = Brand.objects.get(id=pk))
            if form.is_valid():
                form.save()
                messages.success(request,"{} edited successfully".format(Brand.objects.get(id=pk).brand))
                return redirect('brands')
            form = BrandForm(initial = {
                'brand' : Brand.objects.get(id=pk).brand,
                'icon' : Brand.objects.get(id=pk).icon,
                'status' : Brand.objects.get(id=pk).status,

            })
            context = loginPerson(request)
            context['form'] = form

            return render(request,'brand/edit_brand.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('brands')
    else:
        return redirect('login')
    
def customers_disable(request):
    if 'disable/enable customers' in has_permission(request):
        customer_id = request.POST['getById']
        customer_obj = Customer.objects.get(id=customer_id).status
        if customer_obj:
            x = Customer.objects.get(id=customer_id)
            x.status = "inactive"
            x.save()
            return redirect('customers')
        
        
        return render(request,'customers/customers.html')
    else:
        messages.error(request,"access denied")
        return redirect('customers')

def customers_enable(request):
    if 'disable/enable customers' in has_permission(request):
        customer_id = request.POST['getById']
        customer_obj = Customer.objects.get(id=customer_id).status
        print(customer_obj)
        if customer_obj:
            x = Customer.objects.get(id=customer_id)
            x.status = "active"
            x.save()
            return redirect('customers')
        else:
            print("saye,dsff")
            x = Customer.objects.get(id=customer_id)
            x.status = "True"
            x.save()
            return redirect('customers')
    else:
        messages.error(request,"access denied")
        return redirect('customers')
    
    


#_____Product______
def ProductView(request):
    context = loginPerson(request)
    allProduct = Product.objects.all()
    context['cat'] = Category_table.objects.all()
    context['Inventory'] = True
    context['Varient'] = Varient_Type.objects.all()
    if request.method=="POST":
        byProduct = request.POST['byProduct']
        byVarient = request.POST['byVarient']
        byCategory = request.POST['byCategory']
        byStatus = request.POST['byStatus']
        if byProduct:
            allProduct = allProduct.filter(Q(Name__icontains=byProduct))
            context['Filterd'] = True
        if byVarient:
            allProduct = allProduct.filter(Varient_Type=byVarient)
            context['Filterd'] = True
        if byCategory:
            allProduct = allProduct.filter(Product_Category=byCategory)
            context['Filterd'] = True
        if byStatus:
            allProduct = allProduct.filter(status=byStatus)
            context['Filterd'] = True
    
    context['Products'] = Paginator(allProduct.order_by("-id"),page_count).get_page(request.GET.get('page'))  
    return render(request,'Inventory/Products/Products.html',context)




def add_ProductView(request):
    if 'loginId' in request.session:
        if 'add product' in has_permission(request):
            if request.method == "POST":
                VarientFlag = False
                ProductFormData = ProductForm(request.POST,request.FILES)
                if ProductFormData.is_valid():
                    ProducatData=ProductFormData.save()
                    VarientDatas = zip(
                        request.POST.getlist('Varient_name[]'),
                        request.POST.getlist('Varient_Values[]'),
                        request.POST.getlist('Product_stock[]'),
                        request.POST.getlist('Selling_Prize[]'),
                        request.POST.getlist('Display_Prize[]'),
                        )
                    data_dicts = [{
                        'Varient_name': Varient_name,
                        'Varient_Values': Varient_Values,
                        'Product_stock':Product_stock,
                        'Selling_Prize':Selling_Prize,
                        'Display_Prize':Display_Prize,
                        'status':'Active',
                        }
                        for Varient_name, Varient_Values, Product_stock, Selling_Prize, Display_Prize in VarientDatas]
                    for i in data_dicts:
                        print(i)
                    for data in data_dicts:
                        form = ProductVarientForm(data)
                        if form.is_valid():
                            forId=form.save()
                            ProducatData.Product_Items.add(forId)
                            VarientFlag=True
                    if VarientFlag:
                        messages.success(request,'Product added succesfuly')
                        return redirect('ProductView')
                    else:
                        messages.warning(request,'Some issues are detected edit further varients')
                else:
                    print('hi')
                    messages.error(request,'something went wrong try again!!')
                    return redirect('ProductView')                   
            context = loginPerson(request)
            context['form'] = ProductForm()
            context['form_1'] = ProductVarientForm()
            context['Inventory'] = True
            context['Types'] = Varient_Type.objects.all()
            context['offers'] = Offers.objects.all()
            context['Brands'] = Brand.objects.all()
            context['Categories'] = Category_table.objects.all()
            return render(request,'Inventory/Products/addProduct.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('ProductView')
    else:
        return redirect('login')

def varientSelect(request):
    if request.method == "POST":
        typeId = request.POST['subject_id']

        try:
            values = Varient_Values.objects.filter(Varient_Type=typeId,status='Active')
        except:
            data={'error_message':'error'}
            return JsonResponse(data)
        data = {
            'values':list(values.values('id', 'Varient_Values')),
            'Type':Varient_Type.objects.get(id=typeId).Varient_Name,
        }
        return JsonResponse(data)

def disableProduct(request):
    if 'loginId' in request.session:
        if 'disable product' in has_permission(request):
            if request.method == "POST":
                getById=request.POST['getById']
                if getById:
                    if Product.objects.get(id=getById).status == 'Active':
                        Product.objects.filter(id=getById).update(status='Suspended')
                        messages.warning(request,'Disabled')
                        return redirect('ProductView')
                    else:
                        Product.objects.filter(id=getById).update(status='Active')
                        messages.success(request,'Enabled')
                        return redirect('ProductView')
            return redirect('ProductView')
        else:
            messages.error(request,"access denied")
            return redirect('ProductView')
    else:
        return redirect('login')

def viewProduct(request,pk):
    if 'loginId' in request.session:
        if 'view product' in has_permission(request):
            context = loginPerson(request)
            context['ProductDetails'] = Product.objects.get(id=pk)
            context['Inventory'] = True
            context['Thumbnail_images'] = Product_Images.objects.filter(Product_Owner = pk)
            return render(request,'Inventory/Products/viewProduct.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('ProductView')
    else:
        return redirect('login')

def editProduct(request,pk):
    if 'loginId' in request.session:
        if 'edit product' in has_permission(request):
            context = loginPerson(request)
            context['Types'] = Varient_Type.objects.all().filter(status='Active')
            context['form'] = ProductForm(initial={
                'Name':Product.objects.get(id=pk).Name,
                'Description':Product.objects.get(id=pk).Description,
                'Thumbnail_image':Product.objects.get(id=pk).Thumbnail_image,
                'Varient_Type':Product.objects.get(id=pk).Varient_Type,
                'Product_Category':Product.objects.get(id=pk).Product_Category,
                'Product_Brand':Product.objects.get(id=pk).Product_Brand,
                'Product_Items':Product.objects.get(id=pk).Product_Items.all(),
                'status':Product.objects.get(id=pk).status,
            })
            context['Brands'] = Brand.objects.all()
            context['Categories'] = Category_table.objects.all()
            context['data'] = Product.objects.get(id=pk)
            context['Inventory'] = True
            context['product_id'] = pk
            #TODO: sree edit without using form is good 
            if request.method == "POST":
                ProductFormData = ProductForm(request.POST,request.FILES,instance=Product.objects.get(id=pk))
                if request.POST:
                    if ProductFormData.is_valid():
                        ProductFormData.save()
                        messages.success(request,'successfully updated')
                        return redirect('ProductView')
                    else:
                        messages.warning(request,'something went wrong try again later!!')
                        return redirect('ProductView')  
                    
                else:
                    messages.warning(request,'oopps wrong submission')
                    return redirect('ProductView')                       
            return render(request,'Inventory/Products/editProduct.html',context)
        else:
            messages.error(request,"access denied")
            return redirect('ProductView')
    else:
        return redirect('login')

def editProductVarient(request,pk):
    context=loginPerson(request)
    context['form'] = ProductVarientForm(initial={
        'Varient_name' : Product_Varients.objects.get(id=pk).Varient_name,
        'Varient_Values': Product_Varients.objects.get(id=pk).Varient_Values,
        'Product_Offers': Product_Varients.objects.get(id=pk).Product_Offers.all(),
        'Selling_Prize': Product_Varients.objects.get(id=pk).Selling_Prize,
        'Display_Prize': Product_Varients.objects.get(id=pk).Display_Prize,
        'Product_stock': Product_Varients.objects.get(id=pk).Product_stock,
        'status': Product_Varients.objects.get(id=pk).status,
    })
    context['Inventory'] = True
    context['product_id'] = pk
    ProductId = Product.objects.get(Product_Items__id=pk)
    context['product'] = ProductId.id
    
    if request.method=="POST":
        ProductVarientFormData = ProductVarientForm(request.POST,instance=Product_Varients.objects.get(id=pk))
        if ProductVarientFormData.is_valid:
            ProductVarientFormData.save()
            messages.success(request,'successfully updated')
            return redirect('viewProduct',pk=ProductId.id)
        else:
            messages.warning(request,'something went wrong try again later!!')
            return redirect('viewProduct',pk=ProductId.id)  
    return render(request,'Inventory/Products/productVarient_edit.html',context)

def disableProductItem(request):
    print('hi')
    if 'loginId' in request.session:
        if request.method == "POST":
            getById=request.POST['getById']
            ProductId = request.POST['ProId']
            if getById:
                if Product_Varients.objects.get(id=getById).status == 'Active':
                    Product_Varients.objects.filter(id=getById).update(status='Suspended')
                    messages.warning(request,'Disabled')
                    return redirect('viewProduct',pk=ProductId)
                else:
                    Product_Varients.objects.filter(id=getById).update(status='Active')
                    messages.success(request,'Enabled')
                    return redirect('viewProduct',pk=ProductId)
        return redirect('ProductView')
    else:
        return redirect('login')



def addItems(request,pk):
    context = loginPerson(request)
    context['form'] = ProductVarientForm()
    context['Inventory'] = True
    productTypeId = Product.objects.get(id=pk).Varient_Type.id
    context['VarValues'] = Varient_Values.objects.filter(Varient_Type=productTypeId)
    context['ProId'] = pk
    if request.method == "POST":
        ProductVarientFormData = ProductVarientForm(request.POST)
        if ProductVarientFormData.is_valid:
            pData = ProductVarientFormData.save()
            ProductTableData = Product.objects.get(id=pk)
            ProductTableData.Product_Items.add(pData)
            messages.success(request,'successfully Added')
            return redirect('viewProduct',pk=pk)
        else:
            messages.warning(request,'something went wrong try again later!!')
            return redirect('viewProduct',pk=pk)  
    return render(request,'Inventory/Products/addItems.html',context)

def export_csv(request):
    Products = Product_Varients.objects.filter(status="Active",product__status="Active").order_by('-id')
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="ProductList.csv"'
    writer = csv.writer(response)
    writer.writerow(['no:', 'ProductId', 'Product', 'Varient','Stocks'])
    Product_Fields = Products.values_list('id','product__Name','Varient_Values__Varient_Values',)
    count=0
    for pro in Product_Fields:
        count+=1
        writer.writerow([count,pro[0],pro[1],pro[2],None])
    return response

def bulkStocks(request):
    if request.method=="POST":
        csvFile = request.FILES.get('getCSV')
        content = csvFile.read()
        file_content = ContentFile(content)
        file_name = fs.save("temp.csv",file_content)
        temp_file = fs.path(file_name)
        csv_file = open(temp_file, errors='ignore')
        reader = csv.reader(csv_file)
        next(reader)
        Stockflag = False
        for idd,row in enumerate(reader):
            (count,ProductId,Products,Varients,Stocks) = row 
            if len(Stocks) > 0:
                Product_Varients.objects.filter(id=ProductId).update(Product_stock=int(Stocks))
                Stockflag = True
        if Stockflag:
            messages.success(request,'Stock Updated succesfully')
        else:
            messages.info(request,'uploaded CSV file has not have any change')
    return redirect('ProductView')

def search_admin(request):
    if(request.method == "POST"):
        search_name = request.POST.get('customer')
        search_date = request.POST.get('date')
        
        order_obj = Order.objects.all().filter(Q(user__name__icontains=search_name)|Q(order_date__icontains=search_date))
        
    return render(request,"orders/orders.html")

def page_not_found(request,exception):
    return render(request,"404.html")

def admin_log(request):
    context = loginPerson(request)
    context['log_obj'] = Log.objects.all().order_by('-date')
    return render(request,"settings/admin_log.html",context)

def notifications(request):
    context = loginPerson(request)
    notifications = Notifications.objects.all()
    if request.method == "POST":
        filter_title = request.POST['filter_title']
        filter_status = request.POST['filter_status']
        if filter_title:
            notifications = notifications.filter(Q(title__icontains = filter_title))
        if filter_status:
            notifications = notifications.filter(status = filter_status)

    context['notification_obj'] = Paginator(notifications,page_count).get_page(request.GET.get('page'))
    context['notiAct'] = True
    return render(request,"notification/notifications.html",context)

def add_notifications(request):
    if(request.method == "POST"):
        form = NotificationForm(request.POST,request.FILES)
        print(form)
        if form.is_valid():
            form.save()
            messages.success(request,"notification added successfully")
            return redirect('notifications')
    context = loginPerson(request)
    context['form'] = NotificationForm()
    return render(request,"notification/add_notifications.html",context)


def send_notification(request,pk):
    form = NotificationForm(request.POST,request.FILES,instance = Notifications.objects.get(id=pk))
    form.date = date.today()
    if form.is_valid():
        form.date = date.today()
        form.save()
        obj = Notifications.objects.get(id=pk)
        obj.date=date.today()
        obj.save()

        messages.success(request,"notifications sent successfully")
        return redirect('notifications')
   
    context = loginPerson(request)
    context['form'] = NotificationForm(initial={
        'title' : Notifications.objects.get(id=pk).title,
        'description' : Notifications.objects.get(id=pk).description,
        'image' : Notifications.objects.get(id=pk).image,
        'region' : Notifications.objects.get(id=pk).region,

    })
    return render(request,"notification/edit_notification.html",context)

def delete_notification(request,pk):
    Notifications.objects.get(id=pk).delete()
    messages.success(request,"deleted successfully")
    return redirect('notifications')


def Customer_report(request):
    context = loginPerson(request)
    context['Reports'] = True
    all_Customer = Customer.objects.all()
    if request.method=="POST":
        byCustomer=request.POST['byCustomer']
        if byCustomer:
            all_Customer = all_Customer.filter(Q(name__icontains=byCustomer))
    context['Customer_Report'] = Paginator(all_Customer,page_count).get_page(request.GET.get('page'))
    return render(request,'report/cunstomer_report.html',context)

def generateReportCSV(request):
    if 'Customer' in request.GET:
        Reports = Customer.objects.all().order_by('-id')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="CustomerReport.csv"'
        writer = csv.writer(response)
        writer.writerow(['Customer Id', 'Customer', 'E-Mail', 'Phone','Date','Number Of Orders','Amount'])
        Report_Fields = Reports.values_list('id','name','mail','phone','registered_date','Total_Orders','Total_Order_amount')
        count=0
        for pro in Report_Fields:
            count+=1
            writer.writerow(pro)
        return response
    if 'Order' in request.GET:
        Reports = Order.objects.all().order_by('-id')
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="OrderReport.csv"'
        writer = csv.writer(response)
        writer.writerow(['Order Id', 'Customer', 'Contact','Order Date','Order Status','Payment Status','Platform'])
        Report_Fields = Reports.values_list('order_id_m','user__name','user__phone','order_date','order_status','payment_status','platform')
        for pro in Report_Fields:
            if int(pro[6]) == 0: a='Web'
            else: a='App'
            writer.writerow([pro[0],pro[1],pro[2],pro[3],pro[4],pro[5],a])
        return response

def Order_report(request):
    context = loginPerson(request)
    context['Reports'] = True
    all_Orders = Order.objects.all()
    if request.method=="POST":
        byorderID=request.POST['byorderID']
        byCustomer=request.POST['byCustomer']
        byOrderStatus=request.POST['byOrderStatus']
        byPaymentStatus=request.POST['byPaymentStatus']
        if byorderID:
            all_Orders = all_Orders.filter(Q(order_id_m__icontains=byorderID))
        if byCustomer:
            all_Orders = all_Orders.filter(Q(user__name__icontains=byCustomer))
        if byOrderStatus:
            all_Orders = all_Orders.filter(order_status=byOrderStatus)
        if byPaymentStatus:
            all_Orders = all_Orders.filter(payment_status=byPaymentStatus)
    context['Order_Report'] = Paginator(all_Orders,page_count).get_page(request.GET.get('page'))
    return render(request,'report/Order_report.html',context)

def itemsList(request):
    context=loginPerson(request)
    context['Inventory'] = True
    all_Items = ''
    if 'Active' in request.GET:
        print('hi')
        all_Items = Product_Varients.objects.filter(status="Active").order_by('-id')
    elif 'Pending' in request.GET:
        all_Items = Product_Varients.objects.filter(status__in=["Pending","Suspended    "]).order_by('-id')
    print(all_Items)
    print('helo item')
    context['Items'] = Paginator(all_Items,10).get_page(request.GET.get('page'))
    return render(request,'Inventory/Products/items.html',context)

def disableProductItems(request):
    print('hi')
    if 'loginId' in request.session:
        if request.method == "POST":
            getById=request.POST['getById']
            if getById:
                if Product_Varients.objects.get(id=getById).status == 'Active':
                    Product_Varients.objects.filter(id=getById).update(status='Suspended')
                    messages.warning(request,'Disabled')
                    return redirect('itemsList')
                else:
                    Product_Varients.objects.filter(id=getById).update(status='Active')
                    messages.success(request,'Enabled')
                    return redirect('itemsList')
        return redirect('itemsList')
    else:
        return redirect('login')

#--------------ajax-------------
def Display_Order(request):
    data = request.GET.get('p')
    if int(data) in [i.Display_Order for i in Category_table.objects.all()]:
        data = True
    else:
        data = False
    return JsonResponse({'data':data})
def Display_Order_Varient(request):
    data = request.GET.get('p')
    if int(data) in [i.Display_Order for i in Varient_Type.objects.all()]:
        data = True
    else:
        data = False
    return JsonResponse({'data':data})
def VarientName_Check(request):
    data = request.GET.get('p')
    if str(data).lower() in [str(i.Varient_Name).lower() for i in Varient_Type.objects.all()]:
        data = True
    else:
        data = False
    return JsonResponse({'data':data})
def Banner_Display_Order(request):
    data = request.GET.get('p')
    if int(data) in [i.Banner_Display_Order for i in BannerTable.objects.all()]:
        data = True
    else:
        data = False
    return JsonResponse({'data':data})

def User_log(request):
    context = loginPerson(request)
    log = UserLog.objects.all().order_by('-Date')
    if request.method == "POST":
        byUser = request.POST['byUser']
        byFrom = request.POST['byFrom']
        byTo = request.POST['byTo']
        if byUser:
            log = log.filter(Q(User__icontains=byUser))
        if byFrom or byTo:
            if byFrom:
                today = date.today().strftime("%Y-%m-%d")
                log = log.filter(Date__range=(byFrom,today))
            if byTo:
                From = UserLog.objects.all().order_by('Date').first().Date
                log = log.filter(Date__range=(From,byTo))
            if byFrom and byTo:
                log = log.filter(Date__range=(byFrom,byTo))
    context['UserLog'] = Paginator(log,10).get_page(request.GET.get('page'))
    return render(request,'settings/UserLog.html',context)



def render_to_pdf(template_src, context_dict={}):
	template = get_template(template_src)
	html  = template.render(context_dict)
	result = BytesIO()
	pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
	if not pdf.err:
		return HttpResponse(result.getvalue(), content_type='application/pdf')
	return None




      
def Print_invoice(request,pk):
    total = []
    order_obj = Order.objects.get(id=pk)
    order_det_obj = Order_details.objects.all().filter(order_id=order_obj)
    for i in order_det_obj:
        am = i.amount
        total.append(am)
    sum1=0   
    for j in total:
        sum1+=float(j)
    add_obj = Order_details.objects.all().filter(order_id=order_obj)
    for i in add_obj:
        address = i.address.id
        customer = i.customer.id
    data = {
	"company": "Momooz online store",
    
	"address": Address.objects.get(id=address).address,
	"city": Address.objects.get(id=address).district,
	"state": Address.objects.get(id=address).state,
	"zipcode": Address.objects.get(id=address).pin,

    "name": Customer.objects.get(id=customer).name,
	"phone": Customer.objects.get(id=customer).phone,
	"email": Customer.objects.get(id=customer).mail,
	
    "product_name":order_det_obj,
    'total':sum1
	}
    pdf = render_to_pdf('invoice.html', data)
    response = HttpResponse(pdf, content_type='application/pdf')
    
    filename = "Invoice_%s.pdf" %(Order.objects.get(id=pk).order_id_m)
    content = "attachment; filename='%s'" %(filename)
    response['Content-Disposition'] = content
    return response

def contacts(request):
    context = loginPerson(request)
    context['contact_obj'] = Contact_us.objects.all().order_by('-date')

    return render(request,"contact/contacts.html",context)

def edit_customer(request,pk):
    password = Customer.objects.get(id=pk).password
    print(password)
    if(request.method == "POST"):
        customer_obj = CustomerForm(request.POST,instance = Customer.objects.get(id=pk))
        print(customer_obj)
        if customer_obj.is_valid():
            customer_obj.save()
            messages.success(request,"edited successfully")
            return redirect('customers')
    context = loginPerson(request)
    context['edit_form'] = CustomerForm(initial={
        'name' : Customer.objects.get(id=pk).name,
        'mail' : Customer.objects.get(id=pk).mail,
        'phone' : Customer.objects.get(id=pk).phone,
        'password' : Customer.objects.get(id=pk).password,
        'password_confirmation' : Customer.objects.get(id=pk).password_confirmation,
        
    })
    return render(request,'customers/edit_customers.html',context)

def upload_thumnails(request):
    if request.method == "POST":
        data = {'status':False}
        image_file = request.FILES.get('file')
        ProducInfo = request.GET.get('pro')
        if ProducInfo:
            image_qnty = Product_Images.objects.filter(Product_Owner = ProducInfo).count()
            print(image_qnty)
            if int(image_qnty) < 3: 
                item=Product_Images(
                    Product_Owner_id = ProducInfo,
                    Images = image_file
                    )
                item.save()
                listData = list(Product_Images.objects.filter(id=item.id).values('id','Images','Product_Owner'))
                data = {'status':True,'item':listData}
        print(data)
    return JsonResponse(data)

def Thumbnail_remove(request):
    pid = request.GET.get('pm')
    Tumbnail_id = request.GET.get('im')
    images = list(Product_Images.objects.filter(Product_Owner=pid).values('id','Product_Owner','Images'))
    data = {'status':False,'data':images}
    if pid and Tumbnail_id:
        Product_Images.objects.filter(id=Tumbnail_id).delete()
        images = list(Product_Images.objects.filter(Product_Owner=pid).values('id','Product_Owner','Images'))
        data = {'status':True,'data':images}
    return JsonResponse(data,safe=True) 

def disappear_msg_render(request,pk):
    Contact_us.objects.filter(id=pk).update(status=True)
    context = loginPerson(request)
    context['contact_obj'] = Contact_us.objects.filter(id=pk)
    
    return render(request,"contact/contacts.html",context)  

from . send_sms import send_SMS
def forgot_password(request):
    if request.method == "POST" and "otp-validation" in request.POST:
        code = request.POST.getlist("d1")
        code = "".join(str(item) for item in code)
        by_validation = otp_code.objects.get(code_of_admin = request.POST['admin']).code
        if code == by_validation:
            return render(request,'change_password.html',{'admin':Admin.objects.get(id = request.POST['admin'])})
        else:
            print(False)
            
        return redirect('login')
    
    #otp messages
    if request.method == "POST" and "reset-password" in request.POST:
        Username = request.POST['Username']
        Contact = request.POST['Contact']
        try:
            admin = Admin.objects.get(username = Username, phone = Contact)
            existing_code = otp_code.objects.filter(code_of_admin = admin)
            
            if existing_code:
                existing_code.delete()
          
            otp_code(code_of_admin = admin).save()
            code_data = otp_code.objects.get(code_of_admin = admin)
            send_SMS(code_data.code,"+919947242749")
            return render(request,'otp_validation.html',{'admin':admin.id})
        except Exception as e:
            messages.info(request,'Account not existing:'+ str(e))
            return redirect('login')
        
    if request.method == "POST" and "conform-reset-password" in request.POST: 
        admin = request.POST['admin']
        password = request.POST['password']
        confirm_password = request.POST['Confirmpassword']
        if password == confirm_password:
            Admin.objects.filter(id = admin).update(password = password)
            messages.success(request,'Your Password successfuly changed')
            return redirect('login')
    
    return redirect('login')

 

from datetime import date, time
import os
import random
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.auth.models import User
from django.db.models.base import Model
from django.db.models.deletion import CASCADE
from django.db.models.fields import CharField, DateTimeField, TextField
from django.db.models.fields.related import ManyToManyField


class Admin_role(models.Model):
    role_name = models.CharField(max_length=100)
    stat_choice = (
        ('active','active'),
        ('inactive','inactive')
    )
    status = models.CharField(max_length=10,choices=stat_choice)

    def __str__(self) -> str:
        return self.role_name


class RegionTable(models.Model):
    Region_name = models.CharField(max_length=255)
    Short_code = models.CharField(max_length=5,unique=True)
    status = models.CharField(default='Pending',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    Created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'region_table'
    def __str__(self):
        return self.Region_name

class Admin(models.Model):
    code = models.IntegerField(null=True)
    role = models.ForeignKey(Admin_role,on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
   
    email = models.EmailField()
   
    phone = models.CharField(max_length=20)
    
    username = models.CharField(max_length=100,unique=True)
    password = models.CharField(max_length=50)
    stat_choice = (
        ('active','active'),
        ('suspended','suspended'),
    )
    status = models.CharField(max_length=10,choices=stat_choice)
    region = models.ForeignKey(RegionTable,on_delete=models.CASCADE)
    is_superuser = models.BooleanField(auto_created=True,default=False)

class otp_code(models.Model):
    code = models.CharField(max_length=6,blank = True)
    code_of_admin = models.OneToOneField(Admin, on_delete = models.CASCADE)
    class Meta:
        db_table = "otp_code"
    
    def __str__(self) -> str:
        return self.code
    
    def save(self, *args, **kwargs):
        no_list = [i for i in range(10)]
        code_list = []
        for i in range(6):
            num = random.choice(no_list)
            code_list.append(num)
        code_str = "".join(str(item) for item in code_list)
        self.code = code_str
        super().save(*args, **kwargs)

class Main_path(models.Model):
    path_name = models.CharField(max_length=100)
    choice = (
        ('active','active'),
        ('inactive','inactive')
    )
    status = models.CharField(max_length=20,choices=choice)

    def __str__(self) -> str:
        return self.path_name

class Sub_path(models.Model):
    Main_path_id = models.ForeignKey(Main_path,on_delete=models.CASCADE)
    path_name = models.CharField(max_length=100)
    display_order = models.IntegerField(null=True)
    def __str__(self) -> str:
        return self.path_name

class PincodeTable(models.Model):
    Post_Office = models.CharField(max_length=100)
    Pincode = models.CharField(max_length=6)
    Owner = models.ForeignKey(RegionTable,on_delete=models.CASCADE)
    class Meta:
        db_table = "Pincode"
        unique_together = ['Post_Office','Pincode','Owner'] 

    def __str__(self) -> str:
        return self.Pincode

class BannerTable(models.Model):
    Banner_name = models.CharField(max_length =255)
    Banner_URL = models.CharField(max_length=255)
    Banner_Display_Order = models.IntegerField()
    Banner_image = models.FileField(upload_to='banners',blank=True,null=True)
    App_Banner_image = models.FileField(upload_to='banners_app',blank=True,null=True)
    status = models.CharField(default='Pending',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    Created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = 'Banner'



class set_permissions(models.Model):
    role = models.ForeignKey(Admin_role,blank=True,on_delete=models.CASCADE)
    sub_path = models.ForeignKey(Sub_path,blank=True,on_delete=models.CASCADE)
    main_path = models.ForeignKey(Main_path,blank=True,on_delete=models.CASCADE)
  
class Offers(models.Model):
    offer_name = models.CharField(max_length=100)
   
  
    image = models.FileField(upload_to='offers/image')
    App_banar_image = models.FileField(upload_to='offers/banner_image')
    status = models.BooleanField()
  

    def __str__(self) -> str:
        return self.offer_name

class SalesReportTable(models.Model):
    Order_Number = models.CharField(max_length=255)
    Customer = models.CharField(max_length=255)
    Customer_Name = models.CharField(max_length=255)
    Vendor_id = models.CharField(max_length=255)
    Vendor_Name = models.CharField(max_length=255)
    Order_Amount = models.CharField(max_length=255)
    Contact_Number = models.CharField(max_length=255)
    Delivery_Address = models.CharField(max_length=255)
    Order_status = models.CharField(max_length=255)
    Payment_status = models.CharField(max_length=255)
    Order_date = models.CharField(max_length=255)
    Delivery_date = models.CharField(max_length=255)
    Platform = models.CharField(max_length=255)
    Platfom_mode = models.CharField(max_length=255)
    delivery_charge = models.CharField(max_length=255)
    Payment_mode = models.CharField(max_length=255)
    class Meta:
        db_table = "Sales_Report_Table"


class Category_table(models.Model):
    Category_Name = models.CharField(max_length=255)
    Display_Order = models.IntegerField()
    Category_img = models.FileField(upload_to='category') 
    Parent = models.ForeignKey('Category_table',on_delete=CASCADE,null=True,blank=True)
    status = models.CharField(default='Active',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "Category_table"
    def __str__(self) -> str:
        return self.Category_Name

class Varient_Type(models.Model):
    Varient_Name = models.CharField(max_length=255)
    Display_Order = models.IntegerField()
    status = models.CharField(default='Active',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "Varient_Type"
    def __str__(self) -> str:
        return self.Varient_Name

class Varient_Values(models.Model):
    Varient_Values = models.CharField(max_length=255)
    Varient_Type = models.ForeignKey(Varient_Type,on_delete=models.CASCADE)
    Display_Order = models.IntegerField()
    status = models.CharField(default='Active',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "Varient_Values"
    def __str__(self) -> str:
        return self.Varient_Values
class Brand(models.Model):
    brand = models.CharField(max_length=100)
    icon = models.FileField(upload_to='brand/icon')
    status = models.BooleanField(default=True)
    def __str__(self) -> str:
        return self.brand

class Product_Varients(models.Model):
    Varient_name = models.CharField(max_length=255)
    Varient_Values = models.ForeignKey(Varient_Values,on_delete=models.CASCADE)
    Product_Offers = models.ManyToManyField(Offers,blank=True)
    Selling_Prize = models.DecimalField(max_digits=12,decimal_places=2)
    Display_Prize = models.DecimalField(max_digits=12,decimal_places=2)
    Product_stock = models.PositiveIntegerField(blank=True,null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.CharField(default='Active',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    class Meta:
        db_table = "Product_Varients"

class Product(models.Model):
    Name = models.CharField(max_length=250)
    Description = models.TextField()
    Thumbnail_image = models.FileField(upload_to='Products')
    Varient_Type = models.ForeignKey(Varient_Type,on_delete=models.CASCADE)
    Product_Category = models.ForeignKey(Category_table,on_delete=models.CASCADE)
    Product_Brand = models.ForeignKey(Brand,on_delete=models.CASCADE,blank=True,null=True)
    Product_Items = models.ManyToManyField(Product_Varients,verbose_name='ProductItems',blank=True)
    status = models.CharField(default='Active',max_length=20, choices=(
        ('Active','Active'),
        ('Pending','Pending'),
        ('Suspended','Suspended'),
    ))
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "Products"
    def __str__(self) -> str:
        return self.Name

class Product_Images(models.Model):
    Product_Owner = models.ForeignKey(Product,on_delete=models.CASCADE)
    Images = models.FileField(upload_to='Products')
    class Meta:
        db_table = 'Poduct_images'



class Coupon_code(models.Model):
    coupon_code = models.CharField(max_length=100,unique=True)
    from_date = models.DateField()
    to_date = models.DateField()
    type = models.CharField(max_length=15,choices=(
        ('percentage','percentage'),
        ('amount' , 'amount')
    ))
    discount = models.IntegerField(null=True)
    user_count = models.IntegerField(null=True)
    min_amount = models.IntegerField(null=True)
    status = models.BooleanField(default=True,blank=True)



class Customer(models.Model):
    name = models.CharField(max_length=100,unique=True)
    mail = models.EmailField()
    phone = models.CharField(max_length=20,unique=True)
    registered_date = models.DateField(auto_now_add=True)
    password = models.CharField(max_length=100)
    password_confirmation = models.CharField(max_length=100)
    Platform = models.CharField(max_length=2,blank=True,default=0)
    status = models.CharField(max_length=20,choices=(('active','active'),('inactive','inactive'),('deleted','deleted')),default='active',blank=True)
    Total_Orders = models.IntegerField(blank=True,default=0)
    Total_Order_amount = models.DecimalField(max_digits=65,decimal_places=2,blank=True,default=0)
    otp = models.IntegerField(blank=True,null=True)
    class Meta:
        db_table = "Customers"

class Address(models.Model):
    username = models.ForeignKey(Customer,on_delete=models.CASCADE)
    pin = models.ForeignKey(PincodeTable,on_delete=models.CASCADE)
    locality = models.CharField(max_length=100)
    address = models.TextField(max_length=200)
    state = models.CharField(max_length=20,choices=(('kerala','kerala'),))
    district = models.CharField(max_length=20,choices=(('kozhikode','kozhikode'),))
    landmark = models.TextField(max_length=200)
    country = models.CharField(max_length=20,choices=(('india','india'),))
    default = models.BooleanField(default=False)
    address_type = models.CharField(max_length=20,choices=(('home','home'),('work','work'),))

    class Meta:
        db_table = 'address'

class Order(models.Model):
    order_id_m = models.CharField(unique=True,max_length=30)
    order_date = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(Customer,on_delete=models.CASCADE)
    order_total = models.DecimalField(max_digits=20,decimal_places=2)
    discount = models.IntegerField(null=True,blank=True)
    order_status = models.CharField(max_length=20,choices=(
        ('pending','pending'),
        ('failed','failed'),
        ('processing','processing'),
        ('shipped','shipped'),
        ('out for delivery','out for delivery'),
        ('delivered','delivered'),
    ),default='pending')
    payment_status = models.CharField(max_length=20,choices=(
        ('pending','pending'),
        ('received','received'),
        
    ),default='pending')
    platform = models.CharField(max_length=100,null=True,blank=True,default=0)

    #razonpay integrations
    razorpay_payment_id = models.CharField(max_length = 255, null = True, blank = True)
    razorpay_order_id = models.CharField(max_length = 255, null = True, blank = True )
    razorpay_signature = models.CharField(max_length=255, blank = True, null =True)

    def __str__(self):
        return self.order_id_m
    class Meta:
        db_table = 'Order'

class Order_details(models.Model):
    order_id = models.ManyToManyField(Order)
    customer = models.ForeignKey(Customer,on_delete=models.CASCADE)
    product = models.ForeignKey(Product_Varients,on_delete=models.CASCADE)
    address = models.ForeignKey(Address,on_delete=models.CASCADE)
    amount = models.CharField(max_length=100)
    
    count = models.CharField(max_length=100)
    order_date = models.DateField(auto_now_add=True)
    order_time = models.TimeField(auto_now_add=True)
    order_status = models.CharField(max_length=20,choices=(
        ('pending','pending'),
        ('processing','processing'),
        ('shipped','shipped'),
        ('out for delivery','out for delivery'),
        ('delivered','delivered'),
    ),default='pending')
    payment_status = models.CharField(max_length=20,choices=(
        ('pending','pending'),
        
        ('received','received'),
        
    ),default='pending')
    mode = models.CharField(max_length=100,blank=True)

class Log(models.Model):
    user = models.CharField(max_length=100)
    activity = models.TextField()
    date = models.CharField(max_length=30)
    ip_address = models.CharField(max_length=50,blank=True)

class Notifications(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    image = models.FileField(upload_to='notifications')
    region = models.ForeignKey(RegionTable,on_delete=models.CASCADE,blank=True)
    status = models.BooleanField(default=True)
    date = models.CharField(max_length=30,blank=True)







    
from django import forms
from django.db.models import fields
from django.db.models.base import Model
from django.forms import ModelForm
from django.forms import widgets
from django.forms.widgets import CheckboxSelectMultiple, PasswordInput, RadioSelect, Textarea,Widget
from .models import *
from django.core.files.images import get_image_dimensions
class DateInput(forms.DateInput):
    input_type = 'date'


class regionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(regionForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = RegionTable
        fields = ("__all__")
        widgets = {
        'Region_message': Textarea(attrs={'cols': 5, 'rows':4}),
    }

class Admin_roleForm(ModelForm):
    class Meta:
        model = Admin_role
        fields = ("__all__")

    def __init__(self, *args, **kwargs):
        super(Admin_roleForm, self).__init__(*args, **kwargs)
        # self.fields['myfield'].widget.attrs.update({'class': 'myfieldclass'})
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({'class':'form-control'})

class AdminForm(ModelForm):
    class Meta:
        model = Admin
        fields = ("__all__")
        Widget = {
            'password' : PasswordInput()
        }
    
    def __init__(self, *args, **kwargs):
        super(AdminForm, self).__init__(*args, **kwargs)
        # self.fields['myfield'].widget.attrs.update({'class': 'myfieldclass'})
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({'class':'form-control'})

class Sub_pathForm(ModelForm):
    class Meta:
        model = Sub_path
        fields = ("__all__")
    
    def __init__(self, *args, **kwargs):
        super(Sub_pathForm, self).__init__(*args, **kwargs)
        # self.fields['myfield'].widget.attrs.update({'class': 'myfieldclass'})
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({'class':'form-control'})

class Main_pathForm(ModelForm):
    class Meta:
        model = Main_path
        fields = ("__all__")
    
    def __init__(self, *args, **kwargs):
        super(Main_pathForm, self).__init__(*args, **kwargs)
        # self.fields['myfield'].widget.attrs.update({'class': 'myfieldclass'})
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({'class':'form-control'})
       

class PincodeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(PincodeForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = PincodeTable
        fields = ("__all__")

class BannerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(BannerForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = BannerTable
        fields = ("__all__")
    # def clean_picture(self):
    #     picture = self.cleaned_data.get("Banner_image")
    #     print(picture)
    #     if not picture:
    #         raise forms.ValidationError("No image!")
    #     else:
    #         w, h = get_image_dimensions(picture)
    #         if w != 100:
    #             raise forms.ValidationError("The image is %i pixel wide. It's supposed to be 100px" % w)
    #         if h != 200:
    #             raise forms.ValidationError("The image is %i pixel high. It's supposed to be 200px" % h)
    #     return picture

class Set_PermissionForm(ModelForm):
    class Meta:
        model = set_permissions
        fields = ("__all__")


class offersForm(ModelForm):
    class Meta:
        model = Offers
        fields = ("__all__")
       
    
    def __init__(self, *args, **kwargs):
        super(offersForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })

class CategoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CategoryForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = Category_table
        fields = ("__all__")

class Varient_ValuesForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Varient_ValuesForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = Varient_Values
        fields = ("__all__")

class Varient_TypeForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Varient_TypeForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = Varient_Type
        fields = ("__all__")

class Coupon_codeForm(ModelForm):
    class Meta:
        model = Coupon_code
        fields = ("__all__")
        widgets = {
            'from_date': DateInput(),
            'to_date' : DateInput()
        }
    def __init__(self, *args, **kwargs):
        super(Coupon_codeForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
        
class BrandForm(ModelForm):
    class Meta:
        model = Brand
        fields = ("__all__")
    
    def __init__(self, *args, **kwargs):
        super(BrandForm,self).__init__(*args, **kwargs)
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })

class ProductForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = Product
        fields = ("__all__")
        widgets = {
        'Description': Textarea(attrs={'cols': 5, 'rows':4}),
    }

class ProductVarientForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProductVarientForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
    class Meta:
        model = Product_Varients
        fields = ("__all__")
        

class CustomerForm(ModelForm):
    class Meta:
        model = Customer
        fields = ("__all__")
       
    def __init__(self, *args, **kwargs):
        super(CustomerForm,self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = 'username'
        self.fields['mail'].widget.attrs['placeholder'] = 'Email'
        self.fields['phone'].widget.attrs['placeholder'] = 'Phone number'
        self.fields['password'].widget.attrs['placeholder'] = 'Password'
        self.fields['password_confirmation'].widget.attrs['placeholder'] = 'Confirm Password'

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            
            })
    def clean(self):
        cleaned_data = super(CustomerForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("password_confirmation")

        if password != confirm_password:
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )
        
class AddressForm(ModelForm):
    class Meta:
        model = Address
        fields = ("__all__")
   
    def __init__(self, *args, **kwargs):
        super(AddressForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })
        
class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ("__all__")

class Order_detailsForm(ModelForm):
    class Meta:
        model = Order_details
        fields = ("__all__")

class NotificationForm(ModelForm):
    class Meta:
        model = Notifications
        fields = ("__all__")

    def __init__(self, *args, **kwargs):
        super(NotificationForm,self).__init__(*args, **kwargs)

        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control-sm',
            })

  
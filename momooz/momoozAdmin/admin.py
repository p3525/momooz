
from django.contrib import admin
from momoozUser.models import *
from momoozAdmin.models import *

class AdminDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Admin._meta.fields]
    list_display = all_fields
    list_editable = ('is_superuser',)
admin.site.register(Admin,AdminDecoration)

class Admin_roleDecorator(admin.ModelAdmin):
    all_fields = [f.name for f in Admin_role._meta.fields]
    list_display = all_fields
admin.site.register(Admin_role,Admin_roleDecorator)


class OrderDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Order._meta.fields]
    list_display = all_fields
admin.site.register(Order,OrderDecoration)

class OrderDetailsDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Order_details._meta.fields]
    list_display = all_fields
admin.site.register(Order_details,OrderDetailsDecoration)


class AddressDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Address._meta.fields]
    list_display = all_fields
admin.site.register(Address,AddressDecoration)


class CustomerDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Customer._meta.fields]
    list_display = all_fields
admin.site.register(Customer,CustomerDecoration)


class RegionTableDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in RegionTable._meta.fields]
    list_display = all_fields
admin.site.register(RegionTable,RegionTableDecoration)

class Main_pathDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Main_path._meta.fields]
    list_display = all_fields
admin.site.register(Main_path,Main_pathDecoration)

class Sub_pathDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in Sub_path._meta.fields]
    list_display = all_fields
admin.site.register(Sub_path,Sub_pathDecoration)


class set_permissionsDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in set_permissions._meta.fields]
    list_display = all_fields
admin.site.register(set_permissions)

class cart_tableDecoration(admin.ModelAdmin):
    all_fields = [f.name for f in CartTable._meta.fields]
    list_display = all_fields
admin.site.register(CartTable)



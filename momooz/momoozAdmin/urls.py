from django.urls import path
from . import views

urlpatterns = [
    path('',views.login, name='login'),
    path('forgot-password/',views.forgot_password, name='forgot_password'),
    path('logout',views.logout, name='logout'),
    path('home/',views.index, name='index'),
    path('roles',views.roles, name='roles'),
    path('add_roles',views.add_roles, name='add_roles'),
    path('edit_role/<int:pk>',views.edit_role, name='edit_role'),
    path('delete_role/<int:pk>',views.delete_role, name='delete_role'),
    path('set_permission/<int:pk>',views.set_permission, name='set_permission'),
    
    path('add_sub_path',views.add_sub_path, name='add_sub_path'),
    path('add_main_path',views.add_main_path, name='add_main_path'),
    path('path',views.path, name='path'),
    path('Region/',views.Region, name='Region'),
    path('Region/addRegion/',views.addRegion, name='addRegion'),
    path('Region/disableRegion/',views.disableRegion, name='disableRegion'),
    path('Region/editRegion/<str:pk>/',views.editRegion, name='editRegion'),
    path('Region/pincode/<str:pk>/',views.pincode, name='pincode'),
    path('Region/pincode/addPincode',views.addPincode, name='addPincode'),
    path('Region/pincode/disablePincode',views.disablePincode, name='disablePincode'),
    # admins
    path('add_admin',views.add_admin, name='add_admin'),
    path('admins',views.admins, name='admins'),
    path('edit_admins/<int:pk>',views.edit_admins, name='edit_admins'),
    path('view_admins/<int:pk>',views.view_admins, name='view_admins'),
    path('disable_admin',views.disable_admin, name='disable_admin'),
    path('change_password/<int:pk>',views.change_password, name='change_password'),
    # path('momoozAdmin',views.momoozAdmin, name='momoozAdmin'),
    # banners
    path('Banner/',views.Banner, name='Banner'),
    path('Banner/addBanner/',views.addBanner, name='addBanner'),
    path('Banner/disableBanner',views.disableBanner, name='disableBanner'),
    path('Banner/editBanner/<str:pk>',views.editBanner, name='editBanner'),
    # offers
    path('offers',views.offers, name='offers'),
    path('add_offers',views.add_offers, name='add_offers'),
    path('edit_offers/<int:pk>',views.edit_offers, name='edit_offers'),
    path('delete_offers',views.delete_offers, name='delete_offers'),
    path('add-product-to-offers/',views.add_product_to_offers, name='add_product_to_offers'),
    path('SalesReport',views.SalesReport, name='SalesReport'),
    #inventry
    path('Inventory/',views.CategoryView, name='CategoryView1'),
    path('Inventory/CategoryView',views.CategoryView, name='CategoryView'),
    path('Inventory/CategoryView/addCategory',views.addCategory, name='addCategory'),
    path('Inventory/CategoryView/disableCategory',views.disableCategory, name='disableCategory'),
    path('Inventory/CategoryView/editCategory/<str:pk>/',views.editCategory, name='editCategory'),
    path('Inventory/Varient',views.Varient, name='Varient'),
    path('Inventory/Varient/addVarient',views.addVarient, name='addVarient'),
    path('Inventory/Varient/disableVarient',views.disableVarient, name='disableVarient'),
    path('Inventory/Varient/addValues/<str:pk>',views.addValues, name='addValues'),
    path('Inventory/Varient/editVarient/<str:pk>',views.editVarient, name='editVarient'),
    path('Inventory/Varient/editVarient/<str:pk/disableVarientValues',views.disableVarientValues, name='disableVarientValues'),
    path('Inventory/Varient/ViewValues',views.ViewValues, name='ViewValues'),
    path('Inventory/ProductView/',views.ProductView, name='ProductView'),
    path('Inventory/ProductView/add_ProductView',views.add_ProductView, name='add_ProductView'),
    path('Inventory/ProductView/disableProduct',views.disableProduct, name='disableProduct'),
    path('Inventory/ProductView/viewProduct/<str:pk>/',views.viewProduct, name='viewProduct'),
    path('Inventory/ProductView/viewProduct/editProductVarient/<str:pk>/',views.editProductVarient, name='editProductVarient'),
    path('Inventory/ProductView/viewProduct/addItems/<str:pk>/',views.addItems, name='addItems'),
    path('Inventory/ProductView/editProduct/<str:pk>/',views.editProduct, name='editProduct'),
    path('Inventory/ProductView/disableProductItem',views.disableProductItem, name='disableProductItem'),
    path('Inventory/ProductView/disableProductItems',views.disableProductItems, name='disableProductItems'),
    path('Inventory/itemsList/',views.itemsList, name='itemsList'),
    path('upload_thumnails/',views.upload_thumnails, name='upload_thumnails'),
    path('Thumbnail_remove/',views.Thumbnail_remove, name='Thumbnail_remove'),
    #ajax
    path('Display_Order/',views.Display_Order, name='Display_Order'),
    path('varientSelect/',views.varientSelect, name='varientSelect'),
    path('Display_Order_Varient/',views.Display_Order_Varient, name='Display_Order_Varient'),
    path('VarientName_Check/',views.VarientName_Check, name='VarientName_Check'),
    path('Banner_Display_Order/',views.Banner_Display_Order, name='Banner_Display_Order'),
    path('search_admin/',views.search_admin, name='search_admin'),
    # path('CustomerReport',views.CustomerReport, name='CustomerReport'),
    # customers
    path('customers',views.customers, name='customers'),
    path('edit_customer/<str:pk>',views.edit_customer, name='edit_customer'),
    path('customers_disable',views.customers_disable, name='customers_disable'),
    path('customers_enable',views.customers_enable, name='customers_enable'),
    #contacts
    path('contacts',views.contacts, name='contacts'),
    # orders
    path('orders',views.orders, name='orders'),
    path('new_orders',views.new_orders, name='new_orders'),
    # coupons
    path('add_coupon',views.add_coupon, name='add_coupon'),
    path('edit_coupons/<int:pk>',views.edit_coupons, name='edit_coupons'),
    path('coupons',views.coupons, name='coupons'),
    path('coupon_code_users',views.coupon_code_users, name='coupon_code_users'),
    path('delete_coupons/<int:pk>',views.delete_coupons, name='delete_coupons'),
    # brands
    path('brands',views.brands, name='brands'),
    path('add_brand',views.add_brand, name='add_brand'),
    path('delete_brand',views.delete_brand, name='delete_brand'),
    path('edit_brand/<int:pk>',views.edit_brand, name='edit_brand'),
    # 404
    path('page_not_found',views.page_not_found, name='page_not_found'),
    # log
    path('admin_log',views.admin_log, name='admin_log'),
    path('export_csv/',views.export_csv, name='export_csv'),
    path('bulkStocks/',views.bulkStocks, name='bulkStocks'),
    path('notifications/',views.notifications, name='notifications'),
    path('add_notifications/',views.add_notifications, name='add_notifications'),
    path('send_notification/<int:pk>',views.send_notification, name='send_notification'),
    path('delete_notification/<int:pk>',views.delete_notification, name='delete_notification'),
    path('User_log/',views.User_log, name='User_log'),
    #reports
    path('Order_report/',views.Order_report, name='Order_report'),
    path('Customer_report/',views.Customer_report, name='Customer_report'),
    path('generateReportCSV/',views.generateReportCSV, name='generateReportCSV'),
    path('render_to_pdf',views.render_to_pdf),
    path('Print_invoice/<int:pk>', views.Print_invoice, name="Print_invoice"),
    # contact
    path('disappear_msg_render/<int:pk>', views.disappear_msg_render, name="disappear_msg_render"),
]

handler404 = 'momoozAdmin.views.page_not_found'

    
 





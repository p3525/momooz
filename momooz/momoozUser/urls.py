from django.conf.urls import handler404
from django.urls import path
from . import views

urlpatterns = [
    # path('admins/', admin.site.urls),
    path('',views.UserIndex,name='userIndex'),
    path('about',views.about,name='about'),
    path('contact',views.contact,name='contact'),
    path('contact_redirect_page',views.contact_redirect_page,name='contact_redirect_page'),
    path('Categories/',views.Categories,name='Categories'),
    # path('Varients/',views.Varients,name='Varients'),
    
    #Path for ajax
    path('get_subcat/',views.get_subcat,name='get_subcat'),
    # registration
    path('loginpage/',views.loginpage,name='loginpage'),
    path('registerpage/',views.registerpage,name='registerpage'),
    path('forgot_pass/',views.forgot_pass,name='forgot_pass'),
    path('set_new_pass/<int:otp>/',views.set_new_pass,name='set_new_pass'),
    path('send_mail_view/',views.send_mail_view,name='send_mail_view'),
    path('logoutcustomer/',views.logoutcustomer,name='logoutcustomer'),
    path('otp_validation/',views.otp_validation,name='otp_validation'),
    path('otp_expiration/',views.otp_expiration,name='otp_expiration'),
    # account
    path('my_account/',views.my_account,name='my_account'),
    path('cancel_order/<int:pk>',views.cancel_order,name='cancel_order'),
    path('delete_account/',views.delete_account,name='delete_account'),
    path('address/',views.address,name='address'),
    path('delete_address/<int:pk>',views.delete_address,name='delete_address'),
    path('edit_address/',views.edit_address,name='edit_address'),
    path('my_wishlist/',views.my_wishlist,name='my_wishlist'),
    path('my_orders/',views.my_orders,name='my_orders'),
    path('removeWishlist/',views.removeWishlist,name='removeWishlist'),
    path('my_Cart/RemoveCart/',views.RemoveCart,name='RemoveCart'),
    path('Categories/viewProduct/',views.viewProduct,name='viewProduct'),
    path('checkout/',views.checkout,name='checkout'),
    path('order_success/',views.order_success,name='order_success'),
    path('view_orders/<str:pk>',views.view_orders,name='view_orders'),
    path('my_Cart/',views.my_Cart,name='my_Cart'),
    path('cart_checkout/',views.cart_checkout,name='cart_checkout'),
    path('my_Cart/clearCart',views.clearCart,name='clearCart'),
    path('offerWise/<str:pk>/',views.offerWise,name='offerWise'),
    path('checkout_set_promo/',views.checkout_set_promo,name='checkout_set_promo'),

    #Path for ajax
    path('get_subcat/',views.get_subcat,name='get_subcat'),
    path('wishList/',views.wishList,name='wishList'),
    path('validation/',views.validation,name='validation'),
    path('search_product/',views.search_product,name='search_product'),
    path('get_Prize/',views.get_Prize,name='get_Prize'),
    path('Add_To_Cart/',views.Add_To_Cart,name='Add_To_Cart'),
    path('IncreaseQty/',views.IncreaseQty,name='IncreaseQty'),
    path('DecreaseQty/',views.DecreaseQty,name='DecreaseQty'),
    path('Add_To_Cart_View/',views.Add_To_Cart_View,name='Add_To_Cart_View'),
    path('stockCheck/',views.stockCheck,name='stockCheck'),
    path('check_btn_count/',views.check_btn_count,name='check_btn_count'),
    path('filterProduct/',views.filterProduct,name='filterProduct'),
    path('filter_offer_products/',views.filter_offer_products,name='filter_offer_products'),
    path('Product_load_more/',views.Product_load_more,name='Product_load_more'),
    path('Apply_promo/',views.Apply_promo,name='Apply_promo'),
    path('offer_Load_More/',views.offer_Load_More,name='offer_Load_More'),
    path('filterProductSearch/',views.filterProductSearch,name='filterProductSearch'),
    path('razorpay_values/',views.razorpay_values,name='razorpay_values'),

    
]

from django import template
from django.template.defaultfilters import stringfilter
from momoozAdmin.models import Order
from django.utils.safestring import mark_safe
register = template.Library()

@register.simple_tag
def sum1():
    print('hi')
    return Order.order_id_m.count()


@register.simple_tag()
def multiply(qty,price,*args,**kwargs):
    return int(qty)*int(price)

@register.filter()
def to_int(value):
    return int(value)

@register.simple_tag
def to_list(*args):
    return args

@register.simple_tag
def define(val=None):
  return val

@register.simple_tag
def sum(value):
    sum=0
    sum +=value
    return sum

@register.simple_tag
def sum(value):
    sum1 = []
    sum1.append(value)
    total = sum(sum1)
    return total

@register.filter(name='wrap')
@stringfilter
def wrap(value, arg):
    params = arg.split()
    n = int(params[0])
    tag = params[1]
    tagclass = params[2]
    words = value.split(',')
    head = ' '.join( words[:n] )
    tail = ' '.join( words[n:] )
    return mark_safe('<%s class="%s">%s</%s>  <br> %s' % (tag, tagclass, head, tag, tail))
    
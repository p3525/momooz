
from unicodedata import decimal
from django.contrib import messages
from django.db.models.aggregates import Max, Min
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render,redirect
from django.db.models import Q
from django.core.paginator import Paginator
from django.utils import timezone
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from django.template.loader import render_to_string

from datetime import datetime
from datetime import timedelta
import time
from .models import *
import uuid 

from momoozUser.serializers import AddressSerializer
from momoozUser.forms import *

from momoozAdmin.models import *
from momoozAdmin.main import pay_Razorpay
from momoozAdmin.send_sms import send_SMS
from momoozAdmin.main import RAZOR_KEY_ID
from momoozAdmin.main import client as Razor_clint
from momoozAdmin.forms import *

global varient_data
varient_data = None

class Constant_Variables:
    CONST_PHONE = "9539288508"
    CONST_EMAIL = ""
    CONST_LOGO = "User/images/logo.png"
    CONST_FB_URL = ""
    CONST_TWITTER_URL = ""
    CONST_INSTA_URL = ""
    CONST_LIN_URL = ""
    CONST_GMAIL_URL = ""

#SMS CONTENTS
ORDER_FAILIER = "Your Order For {} is failed"
ORDER_SUCCESS = "Your Order For {} is successfully placed"





global add_id
context={}
context['Categories_Parent'] = Category_table.objects.prefetch_related("product_set").filter(Parent=None,status='Active')
context['Phone'] = Constant_Variables.CONST_PHONE
context['Categories'] = Category_table.objects.filter(status='Active')
context['brands'] = Brand.objects.all()
context['VarientTypes'] = Varient_Type.objects.filter(status='Active')
context['Offers'] = Offers.objects.filter(status=True).order_by('-id')
context['maxMinPrice'] = Product_Varients.objects.aggregate(Max('Selling_Prize'),Min('Selling_Prize'))
context['notifications'] = Notifications.objects.all().filter(status = True)
x = [i.id for i in Category_table.objects.filter(Parent=None,status='Active')]
context['Categories_child'] = Category_table.objects.filter(Parent__in=x,status='Active')
context['catAct'] = False
context['offAct'] = False
context['change_price'] = False
context['apply_promo'] = False



  
import time
  
# define the countdown func.
# def countdown(t):
    
#     while t:
#         mins, secs = divmod(t, 60)
#         timer = '{:02d}:{:02d}'.format(mins, secs)
#         print(timer, end="\r")
#         time.sleep(1)
#         t -= 1
      
#     print('Fire in the hole!!')
  
  
# # input time in seconds
# t = input("Enter the time in seconds: ")
  
# # function call
# countdown(int(t)) 
 
  

def customerlogin(request):
    return Customer.objects.get(id=request.session['loginuser'])

def userActivityLog(query,visitedBy):
    log=UserLog(Action=query,Date=datetime.now(),User=visitedBy)
    log.save()

def UserIndex(request):
    global context
    context['Categories'] = Category_table.objects.filter(status='Active')
    context['catAct'] = False
    context['offAct'] = False
    Action = "home page visited"
    context['Categories_Parent'] = Category_table.objects.prefetch_related("product_set").filter(Parent=None,status='Active')
    context['Banners'] = BannerTable.objects.filter(status='Active').order_by("Banner_Display_Order")
    context['Products'] = Product.objects.filter(status = "Active").order_by('-id')
    x = [i.id for i in Category_table.objects.filter(Parent=None,status='Active')]
    context['Categories_child'] = Category_table.objects.prefetch_related("product_set").filter(Parent__in=x,status='Active')
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        context['customerlogin']=customerlogin(request)
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartExistance'] = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        context['wishlist'] = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except Exception as e:
            context['totalCartPrize'] = 0.00
        
        userActivityLog(Action,visitedBy)
        return render(request,'index/userIndex.html',context)
    else:
        visitedBy='Anonymous'
        context['customerlogin'] = None
        context['CartExistance'] = None
        context['totalCartPrize'] = None
        context['wishlist'] = None
        userActivityLog(Action,visitedBy)
        context['Banners'] = BannerTable.objects.filter(status='Active')
        return render(request,'index/userIndex.html',context)
               
def about(request):
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        
        return render(request,"index/about.html",context)   
    else:
        return render(request,"index/about.html",context)   

def contact(request):
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['contact_status'] = False


        if(request.method=="POST"):
        
            submitform = ContactForm(request.POST)
            if submitform.is_valid():
                submitform.save()

                return redirect('contact_redirect_page')
                
                
            
            
        context['form'] = ContactForm() 
        return render(request,"index/contact.html",context) 
    else:
        return render(request,"index/contact.html",context) 

def contact_redirect_page(request):
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        return render(request,"index/contact_redirect.html",context)
    else:
        return render(request,"index/contact_redirect.html")

    

def Categories(request):
    global context
    context['catAct'] = True
    context['offAct'] = False
    visitedBy = 'Anonymous'
    context['wishlist'] = None
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        context['CartExistance'] = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        context['wishlist'] = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00
    else:
        context['customerlogin'] = None
        context['wlCount'] = None
        context['CartCount'] = None
        context['CartExistance'] = None
        context['wishlist'] = None
    context['Varients'] = Varient_Values.objects.filter(status='Active')
    if request.method=='GET':
        Action="View Product Category {}".format(Category_table.objects.get(id=request.GET['pk']))
        primaryKey = request.GET['pk']
        offset = 20
        data = []
        Parents = [i.id for i in Category_table.objects.filter(Parent=None)]
        SubParent=[i.id for i in Category_table.objects.filter(Parent__id__in=Parents)]
        context['Categories_Child'] = [i for i in Category_table.objects.filter(Parent__id=primaryKey)]
        print(context['Categories_Child'])
        if int(primaryKey) in Parents:
            context['subParents'] = [i for i in Category_table.objects.filter(Parent__id=primaryKey)]
            context['childofSubParent'] = [i for i in Category_table.objects.filter(Parent__id__in=[i.id for i in Category_table.objects.filter(Parent__id=primaryKey)])]
            data = [int(primaryKey)]
            SubParent = [i.id for i in Category_table.objects.filter(Parent=primaryKey)]
            childOfSubParent = [i.id for i in Category_table.objects.filter(Parent__id__in=SubParent)]
            productsByCat = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[0:offset]
            context['Products'] = productsByCat                
            if childOfSubParent:
                data.extend(childOfSubParent)
                data.extend(SubParent)
                productsByCat = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[0:offset]
                context['Products'] = productsByCat
            if SubParent:
                data.extend(SubParent)
                productsByCat = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[0:offset]
                context['Products'] = productsByCat
        elif int(primaryKey) in SubParent:
            context['childofSubParent'] = [i for i in Category_table.objects.filter(Parent__id=primaryKey)]
            context['subParents'] = None
            print('subcat')
            data = [int(primaryKey)]
            subOfSubParent = [i.id for i in Category_table.objects.filter(Parent=primaryKey)]
            # print(subOfSubParent)
            if subOfSubParent:
                print('Have child')
                data.extend(subOfSubParent)
                productsByCat = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[0:offset]
                context['Products'] = productsByCat
            else:
                print('no child')
                context['Products'] = Product.objects.filter(Product_Category=primaryKey).filter(status="Active").order_by('-id')[0:offset]
                if context['Products'].count() > offset:context['Offset'] = True
                else:offset:context['Offset'] = False
        else:
            context['childofSubParent'] = [i for i in Category_table.objects.filter(Parent__id__in=[i.id for i in Category_table.objects.filter(Parent__id=primaryKey)])]
            context['subParents'] = [i for i in Category_table.objects.filter(Parent__id=primaryKey)]
            productsByCat = Product.objects.filter(Product_Category=primaryKey).filter(status="Active").order_by('-id')[0:offset]
            context['Products'] = productsByCat
        context['CatDetails'] = Category_table.objects.get(id=primaryKey)
        context['brands_cat'] = list(set([i.Product_Brand for i in context['Products']]))
        userActivityLog(Action,visitedBy)
    return render(request,'category_wise/Products.html',context)

#offset in category and offset limit in Procduct_load_more are change for consumers intrust
#ajax_for_more_load
def Product_load_more(request):
    Action = "Load More Products"
    visitedBy = 'Anonymous'
    offset_limit = 20
    CartExistance=None
    wishlist=None
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        CartExistance = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        wishlist = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
    catId=int(request.POST['cat'])
    offset=int(request.POST['offset'])
    data = []
    Parents = [i.id for i in Category_table.objects.filter(Parent=None)]
    SubParent=[i.id for i in Category_table.objects.filter(Parent__id__in=Parents)]
    ProducList=''
    #category Parent
    if catId in Parents:
        data = [catId]
        SubParent = [i.id for i in Category_table.objects.filter(Parent=catId)]
        childOfSubParent = [i.id for i in Category_table.objects.filter(Parent__id__in=SubParent)]
        ProducList = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[offset:offset+offset_limit]
        ProductCount = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").count()
        if childOfSubParent:
            data.extend(childOfSubParent)
            data.extend(SubParent)
            ProducList = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[offset:offset+offset_limit]
            ProductCount = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").count()
        if SubParent:
            data.extend(SubParent)
            ProducList = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[offset:offset+offset_limit]  
            ProductCount = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").count()
    #category child
    elif catId in SubParent:
        data = [catId]
        subOfSubParent = [i.id for i in Category_table.objects.filter(Parent=catId)]
        # print(subOfSubParent)
        if subOfSubParent:
            data.extend(subOfSubParent)
            ProducList = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").order_by('-id')[offset:offset+offset_limit] 
            ProductCount = Product.objects.filter(Product_Category__id__in=data).filter(status="Active").count()
        else:
            ProducList = Product.objects.filter(Product_Category=catId).filter(status="Active").order_by('-id')[offset:offset+offset_limit] 
            ProductCount = Product.objects.filter(Product_Category=catId).filter(status="Active").count()
    else:
        ProducList = Product.objects.filter(Product_Category=catId).filter(status="Active").order_by('-id')[offset:offset+offset_limit] 
        ProductCount = Product.objects.filter(Product_Category=catId).filter(status="Active").count()
    CatDetails = Category_table.objects.get(id=catId)
    loadingTemplate = render_to_string('Product-list.html',{'Products':ProducList,'CartExistance':CartExistance,'wishlist':wishlist,'CatDetails':CatDetails})
    print(loadingTemplate)
    data={
        'status':True,
        'data':loadingTemplate,
        'totalCount':ProductCount,
    }
    userActivityLog(Action,visitedBy)
    return JsonResponse(data)


def viewProduct(request):
    Action = "Detailed view of Product {}".format(Product.objects.get(id=request.GET['id']).Name)
    this_hour = timezone.now().replace(minute=0, second=0, microsecond=0)
    one_hour_later = this_hour + timedelta(hours=1)
    visitedBy = 'Anonymous'
    context['wishlist'] = None
    context['CartExistance'] = None
    context['catAct'] = True
    context['offAct'] = False
    context['Thumbnail_image'] = Product_Images.objects.filter(Product_Owner=request.GET['id']).order_by('-id')

    if 'loginuser' in request.session:
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        context['wishlist'] = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        cont = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        context['CartExistance'] = cont
    else:
        context['customerlogin'] = None
        context['wlCount'] = None
        context['CartCount'] = None
        context['CartExistance'] = None
        context['wishlist'] = None
        context['totalCartPrize'] = None

    context['last_hour_product_view'] = UserLog.objects.filter(User = visitedBy).order_by('id').only().filter(Date__range = (this_hour,one_hour_later)).filter(Action=Action).count()
    ProId = Product.objects.get(id=request.GET['id']).id
  
    context['Products_count'] = Product.objects.filter(Product_Category=request.GET['cat']).filter(status='Active').order_by('-id').exclude(Product_Items__Product_stock=0).exclude(id=ProId).count()
    context['Products'] = Product.objects.filter(Product_Category_id=int(request.GET['cat'])).filter(status='Active').order_by('-id').exclude(Product_Items__Product_stock=0).exclude(id=ProId)[0:9]
    context['CatDetails'] = Category_table.objects.get(id=request.GET['cat'])
    context['product'] = Product.objects.get(id=request.GET['id'])
    userActivityLog(Action,visitedBy)
    return render(request,'category_wise/Product_View.html',context)
    
    

def registerpage(request):
    context['catAct'] = False
    context['offAct'] = False
    Action = "Visited Registor Page"
    visitedBy = 'Anonymous Visited'
    if(request.method == "POST"):
        name = request.POST['name']
        mail = request.POST['mail']
        phone = request.POST['phone']
        password = request.POST['password']
        password_confirmation = request.POST['password_confirmation']
        Platform = request.POST.get('Platform')
        status = request.POST['status']
        Total_Orders = request.POST.get('Total_Orders')
        Total_Order_amount = request.POST.get('Total_Order_amount')
        print(name,mail,phone,password,password_confirmation,Platform)
        print(status,Total_Orders,Total_Order_amount)
        form = CustomerForm(request.POST)
        Action = "Registration Trubled"
        print('not valid')
        if(form.is_valid()):
            print('valid')
            Action = "New Registration Completed"
            visitedBy=form.cleaned_data['name']
            form.save()
            return redirect('loginpage')
    context['form'] = CustomerForm()
    userActivityLog(Action,visitedBy)
    return render(request,"registration/registerpage.html",context)

        
def loginpage(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'customerlogin' in context:
        context['customerlogin'] = None
        context['wlCount'] = None
        context['CartCount'] = None
        context['totalCartPrize'] = None
    if 'loginuser' in request.session:
        pass
    else:
        
        if(request.method == "POST"):
            name = request.POST['username']
            password = request.POST['password']
            if '@' in name:
                try:
                    customer = Customer.objects.get(mail=name,password=password)
                    if customer.status == 'active':
                        print('active')
                        request.session['loginuser'] = customer.id
                        Action = "Login Succesfully accomplished with e-mail"
                        visitedBy = customer.name
                        userActivityLog(Action,visitedBy)
                        return redirect('userIndex')
                    
                    elif customer.status == 'inactive':
                        print('login--6')
                        messages.error(request,"your account is temporarly dissabled")
                        Action = "Disabled Person who try to login with e-mail"
                        visitedBy = request.POST['username']+"**"+'disabled'
                        userActivityLog(Action,visitedBy)
                        return redirect('loginpage')
                    else:
                        messages.error(request,"invalid login credentials")
                        Action = " Person who tried to login with deleted credentials"
                        visitedBy = request.POST['username']+"**"
                        userActivityLog(Action,visitedBy)
                        return redirect('loginpage')
                except Customer.DoesNotExist:
                    print('login--7')
                    messages.error(request,"invalid login details")
                    customer = None
                    Action = "Anonymous Login try"
                    visitedBy = request.POST['username']+"**"+request.POST['password']
                    userActivityLog(Action,visitedBy)
                    return redirect('loginpage') 
            else:
                print('login--8')
                try:
                    customer = Customer.objects.get(name = request.POST['username'],password=request.POST['password'])
                    print(customer.status)
                    if customer.status:
                        Action = "Login Succesfully accomplished with Username"
                        visitedBy = customer.name
                        userActivityLog(Action,visitedBy)
                        request.session['loginuser'] = customer.id
                        print('hi')
                        return redirect('userIndex')
                    elif customer.status is not True:
                        messages.error(request,"your account is temporarly dissabled")
                        Action = "Disabled Person who try to login with username"
                        visitedBy = request.POST['username']+"**"+'disabled'
                        userActivityLog(Action,visitedBy)
                        return redirect('loginpage')
                    else:
                        messages.error(request,"invalid login credentials")
                        Action = " Person who tried to login with deleted credentials"
                        visitedBy = request.POST['username']+"**"
                        userActivityLog(Action,visitedBy)
                        return redirect('loginpage')
                except Customer.DoesNotExist:
                    customer = None
                    messages.error(request,"invalid login details")                
                    Action = "Anonymous Login try"
                    visitedBy = request.POST['username']+"**"+request.POST['password']
                    userActivityLog(Action,visitedBy)
                    return redirect('loginpage') 
    return render(request,"registration/loginpage.html",context)



def forgot_pass(request):

    if request.is_ajax():
        mail = request.GET.get('data')
        print(mail,"this mail")
        mail_id = [i.id for i in Customer.objects.filter(mail=mail)]
        mail_list = [i.mail for i in Customer.objects.all().filter(mail=mail)]
        
        if mail in mail_list:    
            data = {'mail' :mail_id}

            return JsonResponse(data)
        else:
            data = {'mail' :'invalid mail'}
            return JsonResponse(data)
    data = {'message' :1}
    return JsonResponse(data)

import math, random
def send_mail_view(request):
    print("whrere")
    phone_num = request.GET.get('data')
    print(request.GET.get('action'),"asctfgh")
    link = request.GET.get('link') 
    if(request.GET.get('action')=='send'):
        digits = "123456789"
        OTP = ""
    
    # length of password can be changed
    # by changing value in range
        for i in range(5) :
            OTP += digits[math.floor(random.random() * 9)] 
    
        try:
            otp = Customer.objects.get(phone=phone_num) 
            print(otp,"customer") 
            otp.otp=OTP
            otp.save()
       
           
            
                
        except Exception as e:
            print("exception")
            data = {'status':0}
            return JsonResponse(data)

        content = "your otp for change password is"+OTP
        phone = "+91"+phone_num
        send_SMS(content,phone)
        # send_mail(
        #         subject,
        #         message,
        #         from_email,
        #         cust_mail,

        #         )
    print(request.GET.get('expiry'))
    if(request.GET.get('expiry')=="expired"):
        print("here the action")
        try:
            otp = Customer.objects.get(phone=phone_num) 
            print(otp,"customer") 
            otp.otp=None
            otp.save()    
        except Exception as a :
            pass
    data = {"status":1}
    return JsonResponse(data)

def otp_validation(request):
    print("Sassyen")
    if(request.is_ajax()):
        otp = request.GET.get('otp')
        print("hello sayen",otp)
        cust_object = [i.id for i in Customer.objects.filter(otp=otp)]
        print(cust_object,"objecvt  ")
        if cust_object == "":
            data = {'msg':1}
            return JsonResponse(data)
        else:
            data = {'msg':0}
            return JsonResponse(data)
       
    data = {}    
    return JsonResponse(data)

def set_new_pass(request,otp):
    try:
        customer = Customer.objects.get(otp=otp)
        if(request.method == "POST"):
            password = request.POST['password']
            password_confirm = request.POST['password_confirm']
            try:
                customer = Customer.objects.get(otp=otp)
                customer.password = password  
                customer.password_confirmation=password_confirm
                customer.otp = 0
                customer.save()
                messages.success(request,"changed successfully")
                return redirect('loginpage')
            except Exception as e:
                messages.error(request,"sorry invalid try ")
                return redirect('loginpage')
    except Exception as e:
        messages.error(request,"sorry invalid otp or otp was expired")
        return redirect('loginpage')
    return render(request,"registration/sent_new_pass.html")

def otp_expiration(request):
    data = {}
    return JsonResponse(data)   

def logoutcustomer(request):
    if 'loginuser' in request.session:
        try:
            del request.session['loginuser'] 
            context['customerlogin'] = None
            context['wlCount'] = None
            context['CartCount'] = None
            context['customerlogin']=None
            messages.success(request,"thank you,visit again") 
            return render(request,"registration/loginpage.html")
        except KeyError:
            context['customerlogin'] = None
            messages.success(request,"thank you,visit again") 
        return redirect('loginpage')
    else:
        context['customerlogin'] = None
        return redirect('loginpage')

def my_account(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00


        if(request.method == "POST"):
            form = CustomerForm(request.POST,instance=Customer.objects.get(id=request.session['loginuser']))
            print(form)
            if form.is_valid():
                form.save()
                messages.success(request,"updated")
                return redirect('my_account')
        customer_obj = Customer.objects.get(id=request.session['loginuser'])

        context['customerlogin']=customerlogin(request)
        context['customer_obj'] = customer_obj
        context['form'] = CustomerForm(initial={
            'name' : Customer.objects.get(id=request.session['loginuser']).name,
            'mail' : Customer.objects.get(id=request.session['loginuser']).mail,
            'phone' : Customer.objects.get(id=request.session['loginuser']).phone,
            'registered_date' : Customer.objects.get(id=request.session['loginuser']).registered_date,
            'password' : Customer.objects.get(id=request.session['loginuser']).password,
            'password_confirmation' : Customer.objects.get(id=request.session['loginuser']).password_confirmation,
            'status' : Customer.objects.get(id=request.session['loginuser']).status
        })
        return render(request,"account/my_account.html",context)
    else:
        context['customerlogin'] = None
        context['wlCount'] = None
        context['CartCount'] = None
        context['CartExistance'] = None
        context['wishlist'] = None
        context['totalCartPrize'] = None
        return redirect('loginpage')

def address(request):
    context['catAct'] = False
    context['offAct'] = False
    if(request.is_ajax()):
        global add_id
        add_id = request.GET['id']
        d = Address.objects.get(id=add_id)
        serializeredAd = AddressSerializer(d)
        return JsonResponse(serializeredAd.data)
    
    if(request.method == "POST"):
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "Address added by %s" % visitedBy
        form = AddressForm(request.POST)
        form.default = request.POST.get('default')
        form.address_type = request.POST['address_type']
        if(form.is_valid()):
            add = form.save()
            userActivityLog(Action,visitedBy)
            if form.default is not None:
                Address.objects.filter(username=request.session['loginuser']).exclude(id=add.id).update(default=False)
                userActivityLog('set address id %s default' % add.id,visitedBy)
            messages.success(request,"address added")
            return redirect('address')
    add_obj = Address.objects.all().filter(username=Customer.objects.get(id=request.session['loginuser'])).order_by('-default')
    context['customerlogin']=customerlogin(request)
    context['address'] = add_obj
    context['pin'] = PincodeTable.objects.all().order_by('-id')
    context['form'] = AddressForm(initial={
        'username' : Customer.objects.get(id=request.session['loginuser'])
    })
    return render(request,"account/address.html",context)


def my_wishlist(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        context['my_wishlist'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser'])
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00 
        cont = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        context['CartExistance'] = [i.id for i in Product.objects.filter(Product_Items__id__in=cont)]
        return render(request,'account/mywishlist.html',context)
    else:
        return redirect('loginpage')


def removeWishlist(request):
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "%s Removed from the wishlist" % Product.objects.get(id=request.GET['pk']).Name
        if request.method == "GET":
            pk=request.GET['pk']
            WishListTable.objects.filter(Product_id=pk,WishlistUser=request.session['loginuser']).delete()
            userActivityLog(Action,visitedBy)
            return redirect('my_wishlist')
    else:
        return redirect('loginpage')  


def delete_address(request,pk):
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "Address added by %s" % visitedBy
        add_obj = Address.objects.filter(id=pk)
        next_Default = Address.objects.filter(username=request.session['loginuser']).exclude(id=pk).first()
        if add_obj:
            if Address.objects.get(id=pk).default:
                if next_Default:
                    Address.objects.filter(id=next_Default.id).update(default = True)
            add_obj.delete()
            messages.success(request,"deleted")
            userActivityLog(Action,visitedBy)
            return redirect("address")
        return render(request,"account/address.html")



def edit_address(request):
    if 'loginuser' in request.session:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "Address edited by %s" % visitedBy
        add_id = request.POST['addId']
        if(request.method == "POST"):
            username = Customer.objects.get(id=request.session['loginuser'])
            pin = request.POST['pin1']
            locality = request.POST['locality1']
            address = request.POST['address1']
            state = request.POST['state1']
            district = request.POST['district1']
            country = request.POST['country1']
            landmark = request.POST['landmark1']
            if request.POST.get('default1') == 'on': default = True 
            else: default = False
            address_type = request.POST['address_type1']
            edits = Address.objects.filter(id=add_id)
            if edits:
                edits.update(
                    pin = pin, locality = locality, address = address, state = state,
                    district = district, country = country, landmark = landmark, 
                    address_type = address_type, default = default
                )
                userActivityLog(Action,visitedBy)
                if default:
                    Address.objects.filter(username=request.session['loginuser']).exclude(id=add_id).update(default=False)
                    userActivityLog('set address id %s default' % add_id,visitedBy)
                return redirect('address')       
        return redirect('address')
    return redirect('address')





def my_Cart(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['wishlist'] = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count() 
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00
        data=[i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        Productid = [i for i in Product.objects.filter(Product_Items__id__in=data)]
        context['CartProductVar'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).order_by('-id')
        cart_list=[]
        for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
            
            cart_list.append(i.product_id.product_set.first().id)
     
            y = i.product_id.product_set.first().Product_Category.id
             
        
            context['cat_list'] = Product.objects.all().filter(Product_Category=y,status='Active').exclude(Product_Items__Product_stock=0).exclude(id__in=cart_list)[:15]
            context['cat_list_count'] = Product.objects.all().filter(Product_Category=y,status='Active').exclude(Product_Items__Product_stock=0).exclude(id__in=cart_list).count()
            context['CatDetails'] = Category_table.objects.get(id=y)
            
            context['p'] = Productid
        try:
            context['TotalCart'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser'])
        except:
            pass
        return render(request,'cart/cart.html',context)
    else:
        return redirect('loginpage')

def RemoveCart(request):
    if request.method=="GET":
        cartId = request.GET['pk']
        product = str(CartTable.objects.get(id=cartId).product_id.Varient_Values)+" of "+ str(CartTable.objects.get(id=cartId).product_id.product_set.first().Name)
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "%s deleted from cart by %s" % (product,visitedBy)
        userActivityLog(Action,visitedBy)
        try:
            TotalCartData=Total_Cart_Table.objects.filter(Customer_id=request.session['loginuser'])
            Total_Price = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Price - CartTable.objects.get(id=cartId).Cart_Price
            Total_Offer_Price = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Offer_Price - CartTable.objects.get(id=cartId).Cart_offer_price
            Total_Final_Price = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Final_Price - CartTable.objects.get(id=cartId).Cart_Total_Price
            Total_Quantity = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Quantity - CartTable.objects.get(id=cartId).cart_quantity
            if TotalCartData:
                TotalCartData.update(
                    Total_Price=Total_Price,
                    Total_Offer_Price=Total_Offer_Price,
                    Total_Final_Price=Total_Final_Price,
                    Total_Quantity=Total_Quantity,   
                )
            CartTable.objects.filter(id=cartId).delete()
        except:
            return HttpResponse("<script>alert('Nothing to delete')</script>")
    return redirect('my_Cart')

def clearCart(request):
    CustomId = request.session['loginuser']
    try:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "all the cart items deleted by %s" % visitedBy
        userActivityLog(Action,visitedBy)
        CartTable.objects.filter(Customer_id=CustomId).delete()
        Total_Cart_Table.objects.filter(Customer_id=CustomId).delete()
    except:
        return HttpResponse("alert('Try again Later')")
    return redirect('my_Cart')



def offerWise(request,pk):
    offset = 20
    context['offAct'] = True
    context['catAct'] = False
    context['Varients'] = Varient_Values.objects.filter(status='Active')
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count() 
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00
    Products = Product.objects.filter(status="Active").filter(Product_Items__Product_Offers=pk).order_by('-id')[0:offset]
    context['Products'] = Products
    context['offer'] = Offers.objects.get(id=pk)
    return render(request,'OfferWise/ProductsByItems.html',context)

def offer_Load_More(request):
    offset_limit = 20
    CartExistance=None
    wishlist=None
    if 'loginuser' in request.session:
        CartExistance = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        wishlist = [i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
    offId=int(request.POST['offer'])
    offset=int(request.POST['offset'])
    ProducList = Product.objects.filter(Product_Items__Product_Offers=offId).filter(status="Active").order_by('-id')[offset:offset+offset_limit]
    ProductCount = Product.objects.filter(Product_Items__Product_Offers=offId).filter(status="Active").count()
    loadingTemplate = render_to_string('offer_list.html',{'Products':ProducList,'CartExistance':CartExistance,'wishlist':wishlist})
    data={
        'status':True,
        'data':loadingTemplate,
        'totalCount':ProductCount,
    }

    return JsonResponse(data)


def filter_offer_products(request):
    categories = request.GET.getlist('Category[]')
    Brands = request.GET.getlist('Brand[]') 
    Varients = request.GET.getlist('Varients[]')
    max_price = request.GET.get('max_price')
    min_price = request.GET.get('min_price')
    offer = int(request.GET['offer'])
    CartExistance = None
    wList = None
    if 'loginuser' in request.session:
        CartExistance = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        wList=[i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]

    allProducts = Product_Varients.objects.filter(Product_Offers = offer).filter(status = "Active").order_by('-Selling_Prize')    

    if len(categories) > 0:
        allProducts=allProducts.filter(product__Product_Category__id__in=categories).order_by('-Selling_Prize')
        print(allProducts)
    if len(Brands) > 0:
        allProducts=allProducts.filter(product__Product_Brand__id__in=Brands).order_by('-Selling_Prize')
    if len(Varients) > 0:
        allProducts=allProducts.filter(Varient_Values__id__in=Varients).order_by('-Selling_Prize')


    if max_price and min_price:
        print(max_price,min_price)
        allProducts=allProducts.filter(Selling_Prize__range=(int(min_price), int(max_price)))

    minimum = context['maxMinPrice']['Selling_Prize__min']
    allProducts = allProducts.distinct()
    html_Data = render_to_string('offer_filter.html',{'Products':allProducts,'wishlist':wList,'CartExistance':CartExistance})
    data = {
        'status':True,
        'data':html_Data,
        'min':int(minimum),
    }
    return JsonResponse(data)

#_________ajax____________

def get_subcat(request):
    if request.method == "GET":
        global subcat
        subcat=''
        CatId = request.GET['subject_id']
        try:
            Catogories = Category_table.objects.filter(Parent=CatId,status='Active')
            subCategories = Category_table.objects.filter(Parent__id__in=[i.id for i in Category_table.objects.filter(Parent=CatId)])
            if subCategories:
                try:
                    subcat = list(subCategories.values('id','Category_Name','Parent'))
                except:
                    pass
            data={
                'cat':list(Catogories.values('id', 'Category_Name')),
                'SubCat':subcat,
            }
        except Exception as e:
            data={'error_message':'error'}
            return JsonResponse(data)
    return JsonResponse(data)

def wishList(request):
    if 'loginuser' in request.session:
        if request.method=="GET":
            ProductId=request.GET['subject_id']
            print(ProductId,"yes")
            print([i.Product_id.id for i in WishListTable.objects.all().filter(WishlistUser=request.session['loginuser'])])
            WishlistCount = WishListTable.objects.all().filter(WishlistUser=request.session['loginuser']).count()
            if int(ProductId) in [i.Product_id.id for i in WishListTable.objects.all()] or WishlistCount>=5:
                WishListTable.objects.filter(Product_id=ProductId,WishlistUser=request.session['loginuser']).delete()
                count=WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
                data = {'succes':False,'count':count}
                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                product = Product.objects.get(id=ProductId).Name
                Action = "%s removed from wishlist by %s" % (product,visitedBy)
                userActivityLog(Action,visitedBy)
            else:
                wishlist=WishListTable(
                    Product_id=Product.objects.get(id=ProductId),
                    WishlistUser=Customer.objects.get(id=request.session['loginuser'])
                )
                wishlist.save()
                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                product = Product.objects.get(id=ProductId).Name
                Action = "Added %s to wishlist by %s" % (product,visitedBy)
                userActivityLog(Action,visitedBy)
                count=WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
                print(count)
                data = {'succes':True,'count':count}
    return JsonResponse(data)


def validation(request):
    customer = [ i.name for i in Customer.objects.all()]
    customer_mail = [ i.mail for i in Customer.objects.all()]
    customer_phone = [ i.phone for i in Customer.objects.all()]
    if(request.is_ajax):
        user = request.GET.get('username')
        mail = request.GET.get('email')
        phone = request.GET.get('phone')
    if(user in customer):
        data = {'msg' : "user already exists in this username"}
    elif(mail in customer_mail):
        data = {'msg' : "mail already exists "}
    elif(phone in customer_phone):
        print("this")
        data = {'msg' : "phone already exists "}
    else:
        data = {'msg' : "valid"}
    return JsonResponse(data)


def get_Prize(request):
    if request.method=="GET":
        VarientId=request.GET['subject_id']
        ProductSellingPrice=Product_Varients.objects.get(id=VarientId).Selling_Prize
        ProductDisplayPrice=Product_Varients.objects.get(id=VarientId).Display_Prize
        ProductValue=Product_Varients.objects.get(id=VarientId).Varient_Values.Varient_Values
        ProductValueLable=Product_Varients.objects.get(id=VarientId).Varient_name
        ProductVarientId=Product_Varients.objects.get(id=VarientId).id
        stock=Product_Varients.objects.get(id=VarientId).Product_stock
        if 'loginuser' in request.session:
            CartAddCheck = CartTable.objects.filter(product_id=VarientId,Customer_id=request.session['loginuser'])
        else:
            CartAddCheck = None
        if CartAddCheck:
            CartAdd=False
        else:
            CartAdd=True
        if int(stock) > 0:
            stockStatus = True
        else:
            stockStatus = False
        Product_VarientsStocks = Product_Varients.objects.get(id=VarientId).Product_stock
    data={
        'sellingPrice':ProductSellingPrice,
        'DisplayPrice':ProductDisplayPrice,
        'saves':int(ProductDisplayPrice)-int(ProductSellingPrice),
        'Value':ProductValue,
        'Varientid':ProductVarientId,
        'CartAddStatus':CartAdd,
        'StockStatus':stockStatus,
        'stocks':Product_VarientsStocks,
        'ProductValueLable':ProductValueLable,
        
    }
    return JsonResponse(data)

def Add_To_Cart_View(request):
    if request.method=="GET":
        ProductVarientId = request.GET['VarientId']

        ProductQty = int(request.GET['qty'])
        #ProductId = request.GET['ProductId']
        CartExistance=CartTable.objects.filter(product_id=ProductVarientId,Customer_id=request.session['loginuser'])
        
        customerDetails = Customer.objects.get(id=request.session['loginuser'])
        if CartExistance:
            pass
        else:
            CartDisplayPrice = Product_Varients.objects.get(id=ProductVarientId).Display_Prize
            CartOffer = float(CartDisplayPrice) - float(Product_Varients.objects.get(id=ProductVarientId).Selling_Prize)
            total =  (float(CartDisplayPrice)-float(CartOffer))*ProductQty
            cart=CartTable(Customer_id=customerDetails,
                            product_id = Product_Varients.objects.get(id=ProductVarientId),
                            Cart_Price = CartDisplayPrice,
                            Cart_offer_price = CartOffer*ProductQty,
                            Cart_Total_Price = total,
                            cart_quantity = ProductQty
            )
            cart.save()
            Total_Price=0
            Total_Offer_Price=0
            Total_Final_Price=0
            Total_Quantity=0
            for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
                Total_Price += i.Cart_Price
                Total_Offer_Price += i.Cart_offer_price
                Total_Final_Price += i.Cart_Total_Price
                Total_Quantity += i.cart_quantity
            TotalCartData = Total_Cart_Table.objects.filter(Customer_id=request.session['loginuser'])
            if TotalCartData:
                TotalCartData.update(
                    Total_Price=Total_Price,
                    Total_Offer_Price=Total_Offer_Price,
                    Total_Final_Price=Total_Final_Price,
                    Total_Quantity=Total_Quantity, 
                )
            else:
                TotalCart=Total_Cart_Table(
                    Customer_id = customerDetails,
                    Total_Price=Total_Price,
                    Total_Offer_Price=Total_Offer_Price,
                    Total_Final_Price=Total_Final_Price,
                    Total_Quantity=Total_Quantity,
                )
                TotalCart.save()
        CartCount=CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        try:
            FinalPrize = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Final_Price
        except:
            FinalPrize = 0.00
        data={
            'status':True,
            'cartCount':CartCount,
            'final':FinalPrize,
        }
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        product = Product_Varients.objects.get(id=ProductVarientId).product_set.first().Name
        Action = "%s added to cart from detailed view by %s" % (product,visitedBy)
        userActivityLog(Action,visitedBy)
    return JsonResponse(data)

def Add_To_Cart(request):
    if request.method=="GET":
        ProductVarientId = request.GET['VarientId']
        CartExistance=CartTable.objects.filter(product_id=ProductVarientId,Customer_id=request.session['loginuser'])
        
        customerDetails = Customer.objects.get(id=request.session['loginuser'])
        if CartExistance:
           
            pass
        else:
            CartDisplayPrice = Product_Varients.objects.get(id=ProductVarientId).Display_Prize
            CartOffer = float(CartDisplayPrice) - float(Product_Varients.objects.get(id=ProductVarientId).Selling_Prize) 
            cart=CartTable(Customer_id=customerDetails,
                            product_id = Product_Varients.objects.get(id=ProductVarientId),
                            Cart_Price = CartDisplayPrice,
                            Cart_offer_price = CartOffer,
                            Cart_Total_Price = float(CartDisplayPrice)-float(CartOffer)
            )
            cart.save()
            Total_Price=0
            Total_Offer_Price=0
            Total_Final_Price=0
            Total_Quantity=0
            for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
                Total_Price += i.Cart_Price
                Total_Offer_Price += i.Cart_offer_price
                Total_Final_Price += i.Cart_Total_Price
                Total_Quantity += i.cart_quantity
            TotalCartData = Total_Cart_Table.objects.filter(Customer_id=request.session['loginuser'])
            if TotalCartData:
                TotalCartData.update(
                    Total_Price=Total_Price,
                    Total_Offer_Price=Total_Offer_Price,
                    Total_Final_Price=Total_Final_Price,
                    Total_Quantity=Total_Quantity, 
                )
            else:
                TotalCart=Total_Cart_Table(
                    Customer_id = customerDetails,
                    Total_Price=Total_Price,
                    Total_Offer_Price=Total_Offer_Price,
                    Total_Final_Price=Total_Final_Price,
                    Total_Quantity=Total_Quantity,
                )
                TotalCart.save()
        CartCount=CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        try:
            FinalPrize = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Final_Price
        except:
            FinalPrize = 0.00
        data={
            'status':True,
            'cartCount':CartCount,
            'final':FinalPrize,
        }
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        product = Product_Varients.objects.get(id=ProductVarientId).product_set.first().Name
        Action = "%s added to the cart from the product list by %s" % (product,visitedBy)
        userActivityLog(Action,visitedBy)
    return JsonResponse(data)


def my_orders(request):
    if 'loginuser' in request.session:
        data =  Order.objects.all().filter(user_id=Customer.objects.get(id=request.session['loginuser'])).exclude(order_status='cancelled').order_by('-order_date')
        context['order_det'] = Paginator(data,10).get_page(request.GET.get('page'))
        context['customerlogin']=customerlogin(request)
        context['CartCount']=CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "%s analised his order list" % (visitedBy)
        userActivityLog(Action,visitedBy)
        return render(request,"account/my_orders.html",context) 
    else:
        return redirect('loginpage') 


def search_product(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'loginuser' in request.session:
        if(request.method == "POST"):
            srch = request.POST['search']
            if srch:
                match=Product.objects.filter(Q(Name__icontains=srch))
                context['search_match_count'] = match.count()
                context['search_match'] = match
                context['search_key'] = srch
                context['customerlogin']=customerlogin(request)
                context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
                context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count() 
                try:
                    context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
                except:
                    context['totalCartPrize'] = 0.00
                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                Action = "%s search for %s" % (visitedBy,srch)
                userActivityLog(Action,visitedBy)
        return render(request,"category_wise/search_tets.html",context)
    else:
        if(request.method == "POST"):
            srch = request.POST['search']
            if srch:
                match=Product.objects.filter(Q(Name__icontains=srch)|Q(Product_Brand__brand__icontains=srch))
                visitedBy = 'Anonymous'
                Action = "%s search for %s" % (visitedBy,srch)
                userActivityLog(Action,visitedBy)
                context['search_match'] = match
                context['search_key'] = srch
                context['search_match_count'] = match.count()
            return render(request,"category_wise/search_tets.html",context)


def checkout(request):
    if 'loginuser' in request.session:
        context['catAct'] = False
        context['offAct'] = False
        context['address'] = Address.objects.all().filter(username=Customer.objects.get(id=request.session['loginuser']))
        context['form'] = AddressForm(initial={'username':Customer.objects.get(id=request.session['loginuser'])})
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count() 
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except Exception as e:
            context['totalCartPrize'] = 0.00

        if request.method == "GET": 
            global varient_data
            varient_data = request.GET.get('varient')
            product = Product.objects.get(Product_Items__id=varient_data)
            global object
            object = Product_Varients.objects.get(id=varient_data)
            if object is not None:
                context['object'] = object
                context['products'] = product
                context['quantity'] = request.GET['quantity']
                context['total'] = int(object.Selling_Prize)*int(request.GET['quantity'])
                context['grandtotal'] = int(object.Selling_Prize)*int(request.GET['quantity'])
        

        if(request.method == "POST"):   

            if 'place_order' in request.POST:
                context['promo_code'] = ""
                product_amount = int(Product_Varients.objects.get(id=request.POST.get('product')).Selling_Prize) * int(request.POST.get('count'))
                promo_code = request.POST.get('promo_code')
                coupon_list = [i.coupon_code for i in Coupon_code.objects.all().filter(status=True)]
                if request.POST.get('address',None) is not None:
                    mode = request.POST.get('mode')
                    if len(promo_code)>0 and promo_code in coupon_list:
                        count = Coupon_code.objects.get(coupon_code=request.POST.get('promo_code')).user_count-1
                        customer_count = Coupon_code.objects.get(coupon_code=request.POST.get('promo_code'))
                        customer_count.user_count = count
                        customer_count.save()
                        if mode == "COD":
                            nun = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                            if  nun != 0:
                                order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
                                user = Customer.objects.get(id=request.session['loginuser'])
                                discount = int(Coupon_code.objects.get(coupon_code = promo_code).discount)
                                orderTotal = int(product_amount) - discount
                                form1 = Order(order_id_m=order_id_m,user=user,order_total=orderTotal,discount=discount)
                                form1.save()
                                form2 = Order_details(customer=user, product=Product_Varients.objects.get(id=request.POST.get('product')),address=Address.objects.get(id=request.POST.get('address')) ,amount=request.POST.get('product_amount'),count=request.POST.get('count'),mode=request.POST.get('mode'))
                                form2.save()
                                form2.order_id.add(form1)
                                product = Product_Varients.objects.get(id=request.POST.get('product'))
                                count = request.POST.get('count')
                                if product in Product_Varients.objects.all():
                                    stock_count = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                                    bal = int(stock_count)-int(count)
                                    if bal != 0:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = bal
                                        product_varients.save()
                                    else:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = 0
                                        product_varients.save()
                            else:
                                messages.error(request,"Out of Stock")
                                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                                Action = "Order of %s is aborted caused by the out of stock" % (visitedBy)
                                userActivityLog(Action,visitedBy)
                                return redirect('checkout')
                        elif mode == "Payment":
                            nun = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                            if  nun != 0:
                                order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
                                user = Customer.objects.get(id=request.session['loginuser'])
                                discount = int(Coupon_code.objects.get(coupon_code = promo_code).discount)
                                orderTotal = int(product_amount) - discount
                                payment = pay_Razorpay(orderTotal*100)
                                form1 = Order(order_id_m=order_id_m,user=user,order_total=orderTotal,discount=discount,razorpay_order_id = payment['id'])
                                form1.save()
                                form2 = Order_details(customer=user, product=Product_Varients.objects.get(id=request.POST.get('product')),address=Address.objects.get(id=request.POST.get('address')) ,amount=request.POST.get('product_amount'),count=request.POST.get('count'),mode=request.POST.get('mode'))
                                form2.save()
                                form2.order_id.add(form1)
                                product = Product_Varients.objects.get(id=request.POST.get('product'))
                                count = request.POST.get('count')
                                if product in Product_Varients.objects.all():
                                    stock_count = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                                    bal = int(stock_count)-int(count)
                                    if bal != 0:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = bal
                                        product_varients.save()
                                    else:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = 0
                                        product_varients.save()
                                Details = user
                                payment['razor_key'] = RAZOR_KEY_ID
                                payment['callback_url'] = "http://9pc.in/order_success/"
                                payment['logo'] = Constant_Variables.CONST_LOGO
                                context['payment'] = payment
                                context['Details'] = Details
                                context['total'] = orderTotal
                                context['item'] = Product_Varients.objects.get(id=request.POST.get('product'))
                                context['quantity'] = request.POST.get('count')
                                return render(request,'account/Payment_summery.html',context)
                            else:
                                messages.error(request,"Out of Stock")
                                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                                Action = "Order of %s is aborted caused by the out of stock" % (visitedBy)
                                userActivityLog(Action,visitedBy)
                                return redirect('checkout')
                    else:
                        if mode == "COD":
                            nun = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                            if  nun != 0:
                                order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
                                user = Customer.objects.get(id=request.session['loginuser'])
                                form1 = Order(order_id_m=order_id_m,user=user,order_total=product_amount)
                                form1.save()
                                form2 = Order_details(customer=Customer.objects.get(id=request.session['loginuser']),product=Product_Varients.objects.get(id=request.POST.get('product')),address=Address.objects.get(id=request.POST.get('address')) ,amount=request.POST.get('product_amount'),count=request.POST.get('count'),mode=request.POST.get('mode'))
                                form2.save()
                                form2.order_id.add(form1)
                                product = Product_Varients.objects.get(id=request.POST.get('product'))
                                count = request.POST.get('count')
                                if product in Product_Varients.objects.all():
                                    x = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                                
                                    bal = int(x)-int(count)
                                    if bal != 0:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = bal
                                        product_varients.save()
                                    else:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = 0
                                        product_varients.save()
                            else:
                                messages.error(request,"Out of Stock")
                                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                                Action = "Order of %s is aborted caused by the out of stock" % (visitedBy)
                                userActivityLog(Action,visitedBy)
                                return redirect('checkout')
                            visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                            Action = "Order of %s is succesfully completed by %s" % (str(Product_Varients.objects.get(id=request.POST.get('product')).Selling_Prize)*int(request.POST.get('count')),visitedBy)
                            userActivityLog(Action,visitedBy)
                            return redirect('order_success')
                        elif mode == "Payment":
                            nun = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                            if  nun != 0:
                                payment = pay_Razorpay(product_amount*100)
                                order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
                                user = Customer.objects.get(id=request.session['loginuser'])
                                form1 = Order(order_id_m=order_id_m,user=user,order_total=product_amount,razorpay_order_id = payment['id'])
                                form1.save()
                                form2 = Order_details(customer=Customer.objects.get(id=request.session['loginuser']),product=Product_Varients.objects.get(id=request.POST.get('product')),address=Address.objects.get(id=request.POST.get('address')) ,amount=request.POST.get('product_amount'),count=request.POST.get('count'),mode=request.POST.get('mode'))
                                form2.save()
                                form2.order_id.add(form1)
                                product = Product_Varients.objects.get(id=request.POST.get('product'))
                                count = request.POST.get('count')
                                if product in Product_Varients.objects.all():
                                    x = Product_Varients.objects.get(id=request.POST.get('product')).Product_stock
                                
                                    bal = int(x)-int(count)
                                    if bal != 0:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = bal
                                        product_varients.save()
                                    else:
                                        product_varients = Product_Varients.objects.get(id=request.POST.get('product'))
                                        product_varients.Product_stock = 0
                                        product_varients.save()
                                Details = user
                                payment['razor_key'] = RAZOR_KEY_ID
                                payment['callback_url'] = "http://9pc.in/order_success/"
                                payment['logo'] = Constant_Variables.CONST_LOGO
                                context['payment'] = payment
                                context['Details'] = Details
                                context['total'] = product_amount
                                context['quantity'] = request.POST.get('count')
                                context['item'] = Product_Varients.objects.get(id=request.POST.get('product'))
                                return render(request,'account/Payment_summery.html',context)
                                        
                            else:
                                messages.error(request,"Out of Stock")
                                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                                Action = "Order of %s is aborted caused by the out of stock" % (visitedBy)
                                userActivityLog(Action,visitedBy)
                                return redirect('checkout')       
                else:
                    
                    messages.info(request,"Please Add An Address")
                    return redirect('/address/')
                    # return redirect('/checkout/?quantity=1&varient=%s&buy=' %varient_data)
        return render(request,'account/checkout.html',context)
    else:
        return redirect('loginpage')





def checkout_set_promo(request):
    if request.method=="GET" and request.is_ajax:
        promo_code = request.GET['code']
        coupon_list = [i.coupon_code for i in Coupon_code.objects.all().filter(status=True)]

        if promo_code in coupon_list:
            dataObj = Coupon_code.objects.filter(coupon_code = promo_code).values('coupon_code','discount')
            data = {"status":True,'data':list(dataObj)}

        else:
            data = {"status":False}

        return JsonResponse(data)
    return JsonResponse({"status":False})

@csrf_exempt
def order_success(request):
    context['catAct'] = False
    context['offAct'] = False
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        try:
            context['totalCartPrize'] = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']) 
        except:
            context['totalCartPrize'] = 0.00
        if request.method == "POST":
            try:
                payment_id = request.POST.get('razorpay_payment_id', "")
                razor_order_id = request.POST.get('razorpay_order_id', "")
                razor_signature = request.POST.get('razorpay_signature', "")
                params_dict = {
                    'razorpay_order_id': razor_order_id,
                    'razorpay_payment_id': payment_id,
                    'razorpay_signature': razor_signature
                }

                try:
                    order_db = Order.objects.get(razorpay_order_id = razor_order_id)
                  
                except:
                    HttpResponse('505 Not Found')
                    return render(request,'account/order_error.html',context)
                
                order_db.razorpay_payment_id = payment_id
                order_db.razorpay_signature = razor_signature
                order_db.save()
                result = Razor_clint.utility.verify_payment_signature(params_dict)
               
                if result == None:
                    amount = int(order_db.order_total)*100
                    print(amount,"total")
                    #TODO:sree capture status
                    try:
                        Razor_clint.payment.capture(payment_id,amount)
                        order_db.payment_status = "received"
                        order_db.order_status = "pending"
                        order_db.save()
                        #TODO:sree after token
                        phone = "+91" + Customer.objects.get(id = request.session['loginuser']).phone

                        content = ORDER_SUCCESS.format("Rs."+str(amount/100))
                        send_SMS(content,phone)

                        return render(request,'account/order_success.html',context)
                    except Exception as e:
                        if str(e) == "This payment has already been captured":
                            order_db.payment_status = "received"
                            order_db.order_status = "pending"
                            order_db.save()

                            #TODO:sree after token
                            # phone = "+91" + Customer.objects.get(id = request.session['loginuser']).phone

                            content = ORDER_SUCCESS.format("Rs."+str(amount/100))
                            send_SMS(content,"+919539288508")

                            return render(request,'account/order_success.html',context)
                        else:
                            print("[Payment Error]:",e)
                            content = ORDER_FAILIER.format("Rs."+str(amount/100))
                            send_SMS(content,"+919539288508")
                            order_db.payment_status = "Failed"
                            order_db.order_status = "failed"
                            order_db.save()
                            return render(request,'account/order_error.html',context)

            except Exception as e:
                
                order_db.payment_status = "Failed"
                order_db.order_status = "failed"
                order_db.save()
                content = ORDER_FAILIER.format("Rs."+str(amount/100))
                send_SMS(content,"+919539288508")
                print('[error]:',e)
                return render(request,'account/order_error.html',context)
        return render(request,'account/order_success.html',context)
    else:
        return redirect('loginpage')
    

def view_orders(request,pk):
    if 'loginuser' in request.session:
        context['customerlogin']=customerlogin(request)
        context['catAct'] = False
        context['offAct'] = False
        obj1 = Order.objects.get(order_id_m=pk).id
        context['view_order_obj'] = Order_details.objects.all().filter(order_id=obj1)
        context['id'] = pk
        context['order'] = Order.objects.get(order_id_m=pk)
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "%s analised his order list" % (visitedBy)
        userActivityLog(Action,visitedBy)
        return render(request,"account/view_order.html",context)
    else:
        return redirect('loginpage')


def IncreaseQty(request):
    data = {'status':True}
    if request.method=="GET":
        if request.is_ajax:
            CartId = request.GET['CartId']
            cart_quantity = int(CartTable.objects.get(id=CartId).cart_quantity)+1
            #Cart_Total_Price = float(CartTable.objects.get(id=CartId).Cart_Total_Price) + float(CartTable.objects.get(id=CartId).product_id.Selling_Prize)
            stocks = CartTable.objects.get(id=CartId).product_id.Product_stock
            if (int(stocks) - int(cart_quantity))>=0:
                Cart_Total_Price = cart_quantity * float(CartTable.objects.get(id=CartId).product_id.Selling_Prize)
                Offer = float(CartTable.objects.get(id=CartId).product_id.Display_Prize)-float(CartTable.objects.get(id=CartId).product_id.Selling_Prize)
                CartTable.objects.filter(id=CartId).update(
                    cart_quantity = cart_quantity,
                    Cart_Total_Price = Cart_Total_Price,
                    Cart_offer_price = cart_quantity * Offer
                )
                Total_Price=0
                Total_Offer_Price=0
                Total_Final_Price=0
                Total_Quantity=0
                for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
                    Total_Price += i.Cart_Price
                    Total_Offer_Price += i.Cart_offer_price
                    Total_Final_Price += i.Cart_Total_Price
                    Total_Quantity += i.cart_quantity
                TotalCartData = Total_Cart_Table.objects.filter(Customer_id=request.session['loginuser'])
                if TotalCartData:
                    TotalCartData.update(
                        Total_Price=Total_Price,
                        Total_Offer_Price=Total_Offer_Price,
                        Total_Final_Price=Total_Final_Price,
                        Total_Quantity=Total_Quantity, 
                    )
                else:
                    TotalCart=Total_Cart_Table(
                        Customer_id = Customer.objects.get(id=request.session['loginuser']),
                        Total_Price=Total_Price,
                        Total_Offer_Price=Total_Offer_Price,
                        Total_Final_Price=Total_Final_Price,
                        Total_Quantity=Total_Quantity,
                    )
                    TotalCart.save()
                stockStatus=True
                Cart_Total_Price="{:.2f}".format(Cart_Total_Price)
                cartoffer ="{:.2f}".format(cart_quantity * Offer)
                data = {
                    'Final_Price':Total_Final_Price,
                    'Final_Offer_Price':Total_Offer_Price,
                    'Cart_Total_Price':str(Cart_Total_Price),
                    'cartOffer':str(cartoffer),
                    'stocks':stockStatus,
                }
            else:
                stockStatus=False
                data = {
                    'Final_Price':Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Final_Price,
                    'Final_Offer_Price':Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Offer_Price,
                    'Cart_Total_Price':CartTable.objects.get(id=CartId).Cart_Total_Price,
                    'cartOffer':CartTable.objects.get(id=CartId).Cart_offer_price,
                    'stocks':stockStatus,
                }
            visitedBy = Customer.objects.get(id=request.session['loginuser']).name
            Action = "Quantity of product incremented by %s" % (visitedBy)
            userActivityLog(Action,visitedBy)
    return JsonResponse(data)


def DecreaseQty(request):
    data = {'status':True}
    if request.method=="GET":
        if request.is_ajax:
            if int(request.GET['QntyInput'])<=0:
                pass
            else:
                CartId = request.GET['CartId']
                cart_quantity = int(CartTable.objects.get(id=CartId).cart_quantity)-1
                Cart_Total_Price = float(CartTable.objects.get(id=CartId).Cart_Total_Price) - float(CartTable.objects.get(id=CartId).product_id.Selling_Prize)
                offer = float(CartTable.objects.get(id=CartId).product_id.Display_Prize)-float(CartTable.objects.get(id=CartId).product_id.Selling_Prize)
                CartTable.objects.filter(id=CartId).update(
                    cart_quantity = cart_quantity,
                    Cart_Total_Price = Cart_Total_Price,
                    Cart_offer_price = offer * cart_quantity
                )
                Total_Price=0
                Total_Offer_Price=0
                Total_Final_Price=0
                Total_Quantity=0
                for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
                    Total_Price += i.Cart_Price
                    Total_Offer_Price += i.Cart_offer_price
                    Total_Final_Price += i.Cart_Total_Price
                    Total_Quantity += i.cart_quantity
                TotalCartData = Total_Cart_Table.objects.filter(Customer_id=request.session['loginuser'])
                if TotalCartData:
                    TotalCartData.update(
                        Total_Price=Total_Price,
                        Total_Offer_Price=Total_Offer_Price,
                        Total_Final_Price=Total_Final_Price,
                        Total_Quantity=Total_Quantity, 
                    )
                else:
                    TotalCart=Total_Cart_Table(
                        Customer_id = Customer.objects.get(id=request.session['loginuser']),
                        Total_Price=Total_Price,
                        Total_Offer_Price=Total_Offer_Price,
                        Total_Final_Price=Total_Final_Price,
                        Total_Quantity=Total_Quantity,
                    )
                    TotalCart.save()
                data = {
                    'Final_Price':Total_Final_Price,
                    'Final_Offer_Price':Total_Offer_Price,
                    'Cart_Total_Price':Cart_Total_Price,
                    'cartOffer':cart_quantity * offer,
                }
                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                Action = "Quantity of product decremented %s" % (visitedBy)
                userActivityLog(Action,visitedBy)
    return JsonResponse(data)


#TODO:sayen cart checkout
def cart_checkout(request):
    if 'loginuser' in request.session:
        context['promo_code'] = ""
        context['apply_promo'] = False
        new = []
        context['address'] = Address.objects.all().filter(username=Customer.objects.get(id=request.session['loginuser'])) 
        context['customerlogin']=customerlogin(request)  

        for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
            print(i.product_id.Product_stock)
            print(i.cart_quantity )
            if i.cart_quantity <= i.product_id.Product_stock:
                context['CartProductVar'] = CartTable.objects.filter(Customer_id=request.session['loginuser'])
                x = int(i.Cart_Total_Price)
                sum1 = 0
                sum1 += x
                new.append(sum1)

        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        Action = "%s is going to checkout" % (visitedBy)
        userActivityLog(Action,visitedBy)
                
        context['total'] = sum(new)
        context['customerlogin']=customerlogin(request)    
        context['form'] = AddressForm(initial={'username':Customer.objects.get(id=request.session['loginuser'])})
        context['address'] = Address.objects.all().filter(username=Customer.objects.get(id=request.session['loginuser']))      
        
        context['wlCount'] = WishListTable.objects.filter(WishlistUser=request.session['loginuser']).count()
        context['CartCount'] = CartTable.objects.filter(Customer_id=request.session['loginuser']).count()
        if(request.method=="POST"):

            if 'promo_btn' in request.POST:
                print(sum(new),"dhdhddh")
                global promo_code
                promo_code = request.POST.get('promo_code')
                product_amount = request.POST.get('product_amount')
                coupon_list = [i.coupon_code for i in Coupon_code.objects.all().filter(status=True)]
                if promo_code in coupon_list:
                    if Coupon_code.objects.get(coupon_code=promo_code).user_count > 0:
                        coupon_obj = Coupon_code.objects.get(coupon_code=promo_code).min_amount
                        if coupon_obj <= int(product_amount):
                            coupon_discount = Coupon_code.objects.get(coupon_code=promo_code).discount
                            last_price = int(product_amount)-coupon_discount
                            
                            context['grandtotal'] = last_price
                            context['promo_code'] = request.POST.get('promo_code')
                            context['apply_promo'] = True
                            context['change_price'] = False  
                            context['discount'] = Coupon_code.objects.get(coupon_code=promo_code).discount
                            return render(request,"account/cart_checkout.html",context)
                        else:
                            messages.error(request,"order for Rs{} for getting this coupon code".format(Coupon_code.objects.get(coupon_code=promo_code).min_amount))
                            return render(request,"account/cart_checkout.html",context) 
                    else:
                        messages.error(request,"invalid coupon code")  
                        return render(request,"account/cart_checkout.html",context) 
                else:
                    messages.error(request,"invalid coupon code")
                return render(request,"account/cart_checkout.html",context) 
            
            elif 'cancel' in request.POST:
                context['change_price'] = True   
                context['apply_promo'] = False
                return render(request,"account/cart_checkout.html",context)

            if 'place_order' in request.POST:
                context['promo_code'] = ""
                
                list_total = []
                for i in CartTable.objects.filter(Customer_id=request.session['loginuser']):
                    if i.cart_quantity <= i.product_id.Product_stock:
                        context['CartProductVar'] = CartTable.objects.filter(Customer_id=request.session['loginuser'])
                        x = int(i.Cart_Total_Price)
                        sum1 = 0
                        sum1 += x
                        list_total.append(sum1)
                product_amount = request.POST.get('order_total')
                try:
                    if float(product_amount)<sum(list_total):
                        count = Coupon_code.objects.get(coupon_code=request.POST.get('promo_code')).user_count-1
                        customer_count = Coupon_code.objects.get(coupon_code=request.POST.get('promo_code'))
                        customer_count.user_count = count
                        customer_count.save()
                except ValueError:
                    pass       
                promo_code = request.POST.get('promo_code')
                if promo_code:
                    coupon_obj = Coupon_code.objects.get(coupon_code=promo_code).user_count
                product=request.POST.getlist('product[]')
                print(product,"varient_id")
                count=request.POST.getlist('counts[]')
                order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
                user = Customer.objects.get(id=request.session['loginuser'])
                form1 = Order(order_id_m=order_id_m,user=user,order_total=request.POST.get('order_total'))
                form1.save()
                orderTotal = int(Customer.objects.get(id=request.session['loginuser']).Total_Orders)+1
                Customer.objects.filter(id=request.session['loginuser']).update(Total_Orders=orderTotal,Total_Order_amount=request.POST['order_total'])
                for i in range(len(product)):
                    pro_var = Product_Varients.objects.get(id=product[i])
                    if pro_var.Product_stock !=0:
                        
                        product=request.POST.getlist('product[]')
                        address=request.POST.get('address')
                        amount=request.POST.getlist('product_amount[]')
                        count=request.POST.getlist('counts[]')
                        form2 = Order_details(customer=Customer.objects.get(id=request.session['loginuser']),product=Product_Varients.objects.get(id=product[i]),address=Address.objects.get(id=address) ,amount=amount[i],count=count[i],mode=request.POST.get('mode'))
                        
                        form2.save()
                        
                        form2.order_id.add(form1)

                        cart = CartTable.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser'])).exclude(product_id__Product_stock=0)
                        cart_total = Total_Cart_Table.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser']))
                        cart.delete() and cart_total.delete()
                        x = Product_Varients.objects.get(id=product[i]).Product_stock
                    
                        if( int(x) >= int(count[i])):
                            bal = int(x)-int(count[i])
                            if bal != 0:
                                product_varients = Product_Varients.objects.get(id=product[i])
                                product_varients.Product_stock = bal
                                product_varients.save()

                                
                            else:
                                product_varients = Product_Varients.objects.get(id=product[i])
                                product_varients.Product_stock = 0
                                product_varients.save()
                                
                        else:
                            pass     
                    else:
                        pass
                

                visitedBy = Customer.objects.get(id=request.session['loginuser']).name
                #TODO: sree Hardcoded COD
                Action = "%s succesfuly placed his order as COD" % (visitedBy)
                userActivityLog(Action,visitedBy)
                return redirect('order_success')

        return render(request,"account/cart_checkout.html",context)
    else:
        return redirect('loginpage')



def stockCheck(request):
    if request.method=="GET" and request.is_ajax:
        VarientId = request.GET['subject_id']
        Product_VarientsStocks = Product_Varients.objects.get(id=VarientId).Product_stock
    data={
        'stocks':Product_VarientsStocks
    }
    return JsonResponse(data)

def check_btn_count(request):
    cart_obj = CartTable.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser']))
    cart_data = CartTable.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser'])).count()
    data = {'count' :[i.product_id.Product_stock for i in cart_obj],'cart_count':cart_data}
    return  JsonResponse(data)


def delete_account(request):
    cust_obj = [i for i in Customer.objects.all()]
    if Customer.objects.get(id=request.session['loginuser']) in  cust_obj:
        visitedBy = Customer.objects.get(id=request.session['loginuser']).name
        
        Customer.objects.all().filter(id=request.session['loginuser']).update(status='deleted',registered_date=date.today())
        del request.session['loginuser']
        context['customerlogin'] = None
        Action = "%s deleted their account" % (visitedBy)
        userActivityLog(Action,visitedBy)
        return redirect('userIndex')
    return render(request,"account/my_account.html")

def cancel_order(request,pk):
    order_obj = Order.objects.get(id=pk)
    status = 'cancelled'
    if order_obj:
        order_obj.order_status = status
        order_obj.order_date = datetime.now()
        order_obj.save()
        return redirect("my_orders")
    return render(request,"account/my_orders.html")

def Apply_promo(request):
    if request.method == "POST":
        promo_code = request.POST['promo_code']
        product_amount = request.POST['product_amount']
        coupon_list = [i.coupon_code for i in Coupon_code.objects.all()]
        if promo_code in coupon_list:
            coupon_obj = Coupon_code.objects.get(coupon_code=promo_code).min_amount
            if coupon_obj <= int(product_amount):
                coupon_discount = Coupon_code.objects.get(coupon_code=promo_code).discount
                
                last_price = int(product_amount)-coupon_discount
                
            else:
                
                msg = "sorry this coupon code have minimum amount"
                data = {"msg":msg}
                return JsonResponse(data)
        else:
            
            msg1 = "invalid coupon code"
            data = {"msg":msg1}
            return JsonResponse(data)
    data = {"msg":"hello"}
    return render(request,'account/checkout.html')


def filterProduct(request):
    categories = request.GET.getlist('Category[]')
    Brands = request.GET.getlist('Brand[]') 
    Varients = request.GET.getlist('Varients[]')
    # price = request.GET['price']
    max_price = request.GET.get('max_price')
    min_price = request.GET.get('min_price')
    cat = int(request.GET['cat'])
    CartExistance = None
    wList = None
    if 'loginuser' in request.session:
        CartExistance = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        wList=[i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
    CatDetails = Category_table.objects.get(id=cat)
    data = []
    Parents = [i.id for i in Category_table.objects.filter(Parent=None)]
    SubParent=[i.id for i in Category_table.objects.filter(Parent__id__in=Parents)]
    allProducts=''
    #category Parent
    if cat in Parents:
        data = [cat]
        SubParent = [i.id for i in Category_table.objects.filter(Parent=cat)]
        childOfSubParent = [i.id for i in Category_table.objects.filter(Parent__id__in=SubParent)]
        allProducts = Product_Varients.objects.filter(product__Product_Category__id__in=data).filter(status="Active").order_by('-Selling_Prize')
        print(allProducts)
        if childOfSubParent:
            data.extend(childOfSubParent)
            data.extend(SubParent)
            allProducts = Product_Varients.objects.filter(product__Product_Category__id__in=data).filter(status="Active").order_by('-Selling_Prize')
        if SubParent:
            data.extend(SubParent)
            allProducts = Product_Varients.objects.filter(product__Product_Category__id__in=data).filter(status="Active").order_by('-Selling_Prize') 
    #category child
    elif cat in SubParent:
        data = [cat]
        subOfSubParent = [i.id for i in Category_table.objects.filter(Parent=cat)]
        # print(subOfSubParent)
        if subOfSubParent:
            data.extend(subOfSubParent)
            allProducts = Product_Varients.objects.filter(product__Product_Category__id__in=data).filter(status="Active").order_by('-Selling_Prize')
        else:
            allProducts = Product_Varients.objects.filter(product__Product_Category=cat).filter(status="Active").order_by('-Selling_Prize')
    else:
        allProducts = Product_Varients.objects.filter(product__Product_Category=cat).filter(status="Active").order_by('-Selling_Prize')
    VarientList = None
    #start to filter
    if len(categories) > 0:
        allProducts=allProducts.filter(product__Product_Category__id__in=categories).order_by('-Selling_Prize')
    if len(Brands) > 0:
        allProducts=allProducts.filter(product__Product_Brand__id__in=Brands).order_by('-Selling_Prize')
    if len(Varients) > 0:
        allProducts=allProducts.filter(Varient_Values__id__in=Varients).order_by('-Selling_Prize')
        VarientList=list(map(int, Varients))

    # print(price)
    # if int(price) > 0:
    #     allProducts=allProducts.filter(Selling_Prize__range=(0, int(price)))
    if max_price and min_price:
        allProducts=allProducts.filter(Selling_Prize__range=(int(min_price), int(max_price)))
    minimum = context['maxMinPrice']['Selling_Prize__min']
    
    allProducts = allProducts.distinct()
    html_Data = render_to_string('test.html',{'Products':allProducts,'wishlist':wList,'CartExistance':CartExistance,'CatDetails':CatDetails,'VarientList':VarientList})
    data = {
        'status':True,
        'data':html_Data,
        'min':int(minimum),
    }
    return JsonResponse(data)

    
def filterProductSearch(request):
    categories = request.GET.getlist('Category[]')
    Brands = request.GET.getlist('Brand[]') 
    Varients = request.GET.getlist('Varients[]')
    max_price = request.GET.get('max_price')
    min_price = request.GET.get('min_price')
    search_key = request.GET['search_key']
    CartExistance = None
    wList = None
    if 'loginuser' in request.session:
        CartExistance = [i.product_id.id for i in CartTable.objects.filter(Customer_id=request.session['loginuser'])]
        wList=[i.Product_id.id for i in WishListTable.objects.filter(WishlistUser=request.session['loginuser'])]
    allProducts = Product_Varients.objects.filter(Q(product__Name__icontains=search_key)).order_by('-Selling_Prize')
    print(allProducts)
    if len(categories) > 0:
        allProducts=allProducts.filter(product__Product_Category__id__in=categories).order_by('-Selling_Prize')
    if len(Brands) > 0:
        allProducts=allProducts.filter(product__Product_Brand__id__in=Brands).order_by('-Selling_Prize')
    if len(Varients) > 0:
        print('varients: ',Varients)
        allProducts=allProducts.filter(Varient_Values__id__in=Varients).order_by('-Selling_Prize')

    if max_price and min_price:
        allProducts=allProducts.filter(Selling_Prize__range=(int(min_price), int(max_price)))
    minimum = context['maxMinPrice']['Selling_Prize__min']
    
    allProducts = allProducts.distinct()
    html_Data = render_to_string('offer_filter.html',{'Products':allProducts,'wishlist':wList,'CartExistance':CartExistance})
    data = {
        'status':True,
        'data':html_Data,
        'min':int(minimum),
    }
    return JsonResponse(data)
    
#TODO:sayen
def razorpay_values(request):
    
    promo_code = ""
    varient_id_razor = []
    amount = []
    cart_count = []
    cart_count = [i.cart_quantity for i in CartTable.objects.all().filter(Customer_id=request.session['loginuser'])]
    amount = [l.Cart_Price for l in CartTable.objects.all().filter(Customer_id=request.session['loginuser'])]
    amount2 = Total_Cart_Table.objects.get(Customer_id=request.session['loginuser']).Total_Final_Price
    varient_id_razor = [j.product_id.id for j in CartTable.objects.all().filter(Customer_id=request.session['loginuser'])] 
    address = request.GET.get('address')
    amount_promo = request.GET.get('amount_promo')
    promo_code = request.GET.get('promo_code')
    if promo_code != "":
        payment = pay_Razorpay(int(amount_promo)*100)
    else:
        payment = pay_Razorpay(int(amount2)*100)
    mode = request.GET.get('mode')
    user_data = [i.name for i in Customer.objects.filter(id=request.session['loginuser'])]
    try:
        if float(amount_promo)<amount2:
            coupon_user_count = Coupon_code.objects.get(coupon_code=promo_code).user_count-1
            customer_count = Coupon_code.objects.get(coupon_code=promo_code)
            customer_count.user_count = coupon_user_count
            customer_count.save()
    except Exception as e:
        pass       
 
    if promo_code != "":
        discount = Coupon_code.objects.get(coupon_code=promo_code).discount
        order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
        user = Customer.objects.get(id=request.session['loginuser'])
        form1 = Order(order_id_m=order_id_m,user=user,order_total=amount_promo,razorpay_order_id = payment['id'],discount=discount)
        form1.save()
        orderTotal = int(Customer.objects.get(id=request.session['loginuser']).Total_Orders)+1
        Customer.objects.filter(id=request.session['loginuser']).update(Total_Orders=orderTotal,Total_Order_amount=amount2)
    else:
        
        order_id_m = "MZID"+uuid.uuid4().hex[:5].upper()
        user = Customer.objects.get(id=request.session['loginuser'])
        form1 = Order(order_id_m=order_id_m,user=user,order_total=amount2,razorpay_order_id = payment['id'])
        form1.save()
        orderTotal = int(Customer.objects.get(id=request.session['loginuser']).Total_Orders)+1
        Customer.objects.filter(id=request.session['loginuser']).update(Total_Orders=orderTotal,Total_Order_amount=amount2)
        

    for i in range(len(varient_id_razor)):
        pro_var = Product_Varients.objects.get(id=varient_id_razor[i])
        if pro_var.Product_stock !=0:
            
           
            form2 = Order_details(
                customer=Customer.objects.get(id=request.session['loginuser']),
                product=pro_var,
                address=Address.objects.get(id=address),
                amount=pro_var.Selling_Prize,
                count=cart_count[i],
                mode=mode
                )
            form2.save()
            form2.order_id.add(form1)

            cart = CartTable.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser'])).exclude(product_id__Product_stock=0)
            cart_total = Total_Cart_Table.objects.all().filter(Customer_id=Customer.objects.get(id=request.session['loginuser']))
            cart.delete() and cart_total.delete()
            x = Product_Varients.objects.get(id=varient_id_razor[i]).Product_stock
        
            if( int(x) >= int(cart_count[i])):
                bal = int(x)-int(cart_count[i])
                if bal != 0:
                    product_varients = Product_Varients.objects.get(id=varient_id_razor[i])
                    product_varients.Product_stock = bal
                    product_varients.save()

                    
                else:
                    product_varients = Product_Varients.objects.get(id=varient_id_razor[i])
                    product_varients.Product_stock = 0
                    product_varients.save()
                    
            else:
                pass     
        else:
            pass

        
    data = {
        'razor_key' : RAZOR_KEY_ID,
        'callback_url' : "http://127.0.0.1:8000/order_success/",
        'logo' : Constant_Variables.CONST_LOGO,
        'payment' : payment,
        'Details' : user_data,
        'email' : Customer.objects.get(id=request.session['loginuser']).mail,
        'phone' : Customer.objects.get(id=request.session['loginuser']).phone
    }
    return JsonResponse(data)
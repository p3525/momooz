from email.policy import default
from django.db import models
from momoozAdmin.models import *

# Create your models here.

class WishListTable(models.Model):
    Product_id = models.ForeignKey(Product,on_delete=models.CASCADE)
    WishlistUser = models.ForeignKey(Customer,on_delete=models.CASCADE,blank=True,null=True)
    added_date = models.DateTimeField(auto_now_add=True)
    class Meta:
        db_table = "Wishlist"

class Total_Cart_Table(models.Model):
    Customer_id = models.ForeignKey(Customer,on_delete=models.CASCADE)
    Total_Price = models.DecimalField(max_digits=12,decimal_places=2)
    Total_Offer_Price = models.DecimalField(max_digits=12,decimal_places=2)
    Total_Final_Price = models.DecimalField(max_digits=12,decimal_places=2)
    Total_Quantity = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now=True, verbose_name='created')
    class Meta:
        db_table="Total_cart"


class CartTable(models.Model):
    Customer_id = models.ForeignKey(Customer,on_delete=models.CASCADE)
    cart_quantity = models.PositiveIntegerField(default=1,blank=True)
    Cart_Price = models.DecimalField(max_digits=12,decimal_places=2,null=True)
    Cart_offer_price = models.DecimalField(max_digits=12,decimal_places=2,null=True)
    Cart_Total_Price = models.DecimalField(max_digits=12,decimal_places=2,null=True)
    product_id = models.ForeignKey(Product_Varients,on_delete=models.CASCADE)
    available = models.BooleanField(default=True, verbose_name="available")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, verbose_name='updated')
    
    class Meta:
        db_table = 'Cart_table'


class UserLog(models.Model):
    
    Action = models.CharField(max_length=500,blank=True)
    Date = models.CharField(max_length=500,blank=True)
    User = models.CharField(max_length=255,blank=True)

    class Meta:
        db_table = 'User_Log'

class Contact_us(models.Model):

    
    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=20)
    message = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False,blank=True)
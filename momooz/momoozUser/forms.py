from django import forms
from django.db.models import fields
from django.db.models.base import Model
from django.forms import ModelForm
from django.forms import widgets
from django.forms.widgets import CheckboxSelectMultiple, PasswordInput, RadioSelect, Textarea,Widget
from .models import *

class CartForm(ModelForm):
    class Meta:
        model = CartTable
        fields = ("__all__")

class ContactForm(ModelForm):
    class Meta:
        model = Contact_us
        fields = ("__all__")

    def __init__(self, *args, **kwargs):
        super(ContactForm,self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = 'Name'
        self.fields['last_name'].widget.attrs['placeholder'] = 'Last name'
        self.fields['email'].widget.attrs['placeholder'] = 'Email'
        self.fields['phone'].widget.attrs['placeholder'] = 'Phone number'
        
        self.fields['message'].widget.attrs['placeholder'] = 'Message'
        for name in self.fields.keys():
            self.fields[name].widget.attrs.update({
                'class':'form-control form-control--sm',
            })  
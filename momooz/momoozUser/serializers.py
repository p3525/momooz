from momoozAdmin.models import *
from rest_framework import serializers

class ProductItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product_Varients
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    Items = ProductItemsSerializer(read_only=True, many=True)
    class Meta:
        model = Product
        fields = '__all__'

class pinCodeSerializer(serializers.ModelSerializer):
    class Meta:
        model = PincodeTable
        fields = '__all__'

class AddressSerializer(serializers.ModelSerializer):
    pin = pinCodeSerializer(read_only = True, many=False)
    class Meta:
        model = Address
        fields = '__all__'

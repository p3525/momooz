from django.apps import AppConfig


class MomoozuserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'momoozUser'
